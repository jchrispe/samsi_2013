"""
 Retention Basin Characteristics from Travis and Mays 08 Retention Basin optimization example
"""
import numpy
cimport numpy
import sys

import subsurface_hydrology as gw

cdef extern from "math.h":
    double pow(double x, double y)
#

def maximum_basin_area_from_hydrology(double H,double Y,double Psi,double K,double delTheta):
    """
    Maximum allowed basin area based on requirement that subsurface seepage rate exceeds or
     matches infiltration rate from basin

    Note if K >= f_a (average infiltration rate is less than saturated conductivity), then
       assume that get direct recharge to the groundwater table and don't build up saturated region
       so no constraint on retention basin

    H   -- depth to water table [L]
    Y   -- Ponded depth [L]
    Psi -- Suction at wetting front [L]
    K   -- Saturated Hydraulic Conductivity [L/T]
    delTheta -- difference between saturated volume fraction and initial volume fraction

    
    """
    cdef double f_a = gw.average_infiltration_rate(Y,Psi,K,delTheta)
    if K >= f_a:
        return sys.float_info.max
    
    cdef A_hmax = 0.5*H*H*(K/f_a + 1.0)/((f_a/K - 1.0)*(f_a/K - 1.0))

    return A_hmax

def cost(double L, double A, double C1, double C2, double S):
    """
    Estimate cost for a retention basin based on purchase and construction

    L  -- price per area [$/L^2]
    A  -- area of basin
    C1 -- cost coefficient, [$/L^3] in [0.02,0.4]
    C2 -- cost coefficient, in [0.63,0.75]
    S  -- basin storage [L^3]
    """

    cdef double cost = L*A + C1*pow(S,C2)

    return cost

    
