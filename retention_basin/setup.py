from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
import numpy,os

setup(
    cmdclass = {'build_ext': build_ext},
    ext_modules = [Extension(name="subsurface_hydrology",
                             sources=["subsurface_hydrology.pyx"],
                             include_dirs=[numpy.get_include()]),
                   Extension(name="surface_hydrology",
                             sources=["surface_hydrology.pyx"],
                             include_dirs=[numpy.get_include()]),
                   Extension(name="retention_basin",
                             sources=["retention_basin.pyx"],
                             include_dirs=[numpy.get_include()])
                   ]
)
