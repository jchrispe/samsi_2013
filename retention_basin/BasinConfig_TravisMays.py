# Example Basin Set-Up Script. This makes sense to be done in parts, and 
# to be written in python form the start.  
"""
# There are 17 parameters for each network node. 
# Here we use the (isodrainage line,index) convention:
#
#          (4,1)           : Travis and Mays Network
#         /    \           : at left.  
#     (3,2)  (3,1)         :
#      /  \      \
#  (2,3) (2,2)  (2,1)
#                  \
#                 (1,1) 
# ========================================================================= #
"""

import networkx as nx

# -------------------------------------------------------------------------- # 
# Set the name for each of the nodes in the network set in build topology.   #
# This will use the (isodrainage line,index) convention.                     #
# -------------------------------------------------------------------------- #

node_list = [(1,1),(2,1),(2,2),(2,3),(3,1),(3,2),(4,1)] 

G = nx.DiGraph()

G.add_nodes_from(node_list)

# ========================================================================== #
# Add network edges with the flow direction being upstream. This is the      #
# reverse of parent child relationship in tree.                              #
# ========================================================================== #

G.add_edge((1,1),(2,1)); G.add_edge((2,1),(3,1)); G.add_edge((3,1),(4,1))
G.add_edge((2,2),(3,2)); G.add_edge((2,3),(3,2)); G.add_edge((3,2),(4,1))

# -------------------------------------------------------------------------- # 
# Set up a blank dictionary for basin, watershed, and subsurface properties. #
# -------------------------------------------------------------------------- #

retention_basin_properties = {}; 
watershed_properties       = {}; 
subsurface_properties      = {};

# -------------------------------------------------------------------------- # 
# For each node in the network define (17) dictionary properties.            #
# -------------------------------------------------------------------------- #
# (BASIN)
# 'L'              ==>  $/M^2 Cost of Land (per area)
# 'C_1'            ==>  $/M^3 Cost of Storage (per volume water)
# 'C_2'            ==>  Cost coefficeint
# 'A_bmax'         ==>  M^2 Max Area given site constraints
# 'Y_jmax'         ==>  M Max Ponding Depth based on 'regulation'
# 't_dmin'         ==> [h] minimum time to drain
# 't_dmax':36}     ==> [h] maximum time to drain
# (WATERSHED)
# 'A_w'            ==> M^2 Area of Watershed
# 'IDF_a'          ==> Rainfall intensity - Duration- Frequency coefficient
# 'IDF_b'          ==> [h] Rainfall intensity - Duration- Frequency coefficient
# 'IDF_e'          ==> Rainfall intensity - Duration- Frequency coefficient
# 'I_m'            ==> [m/h] Intensity of design storm 
# 'C':0.51}        ==> [-] Runoff coefficient
# (SUBSURFACE)
# 'Psi'            ==> [m] Suction at infiltration front
# 'K'              ==> [m/h] Hydraulic Conductivity
# 'Delta_Theta'    ==> difference in Saturated Volume Fraction and initial moisture content --> not specified in paper!
# 'H'              ==> [m] Depth to water table


# (1,1):
retention_basin_properties[(1,1)] = {'L':30.0, 'C_1':0.05, 'C_2':0.75, 'A_bmax':1010., 'Y_jmax':1.2,'t_dmin':12.,'t_dmax':36}
watershed_properties[(1,1)]       = {'A_w': 4050.0, 'IDF_a': 114., 'IDF_b': 15.0/60., 'IDF_e':1.10, 'I_m':7.6*0.01, 'C':0.51}
subsurface_properties[(1,1)]      = {'Psi':0.1, 'K':0.03, 'Delta_Theta':0.3, 'H':18.3}

# (2,1):
retention_basin_properties[(2,1)] = {'L':40.0, 'C_1':0.05, 'C_2':0.75, 'A_bmax':1110., 'Y_jmax':1.2, 't_dmin':12., 't_dmax':36}
watershed_properties[(2,1)]       = {'A_w': 4450.0, 'IDF_a': 114., 'IDF_b': 15.0/60., 'IDF_e':1.10, 'I_m':7.6*0.01, 'C':0.62}
subsurface_properties[(2,1)]      = {'Psi':0.08, 'K':0.02, 'Delta_Theta':0.3, 'H':18.3} 
 
# (2,2):
retention_basin_properties[(2,2)] = {'L':30.0, 'C_1':0.05, 'C_2':0.75, 'A_bmax':2430., 'Y_jmax':1.2, 't_dmin':12., 't_dmax':36}
watershed_properties[(2,2)]       = {'A_w': 9710.0, 'IDF_a': 114., 'IDF_b': 15.0/60., 'IDF_e':1.10, 'I_m':7.6*0.01, 'C':0.62}        
subsurface_properties[(2,2)]      = {'Psi':0.09, 'K':0.02, 'Delta_Theta':0.3, 'H':18.3} 

# (2,3):                        
retention_basin_properties[(2,3)] = {'L':40.0, 'C_1':0.05, 'C_2':0.75, 'A_bmax':1820., 'Y_jmax':1.2, 't_dmin':12., 't_dmax':36}
watershed_properties[(2,3)]       = {'A_w': 7280.0, 'IDF_a': 114., 'IDF_b': 15.0/60., 'IDF_e':1.10, 'I_m':7.6*0.01, 'C':0.62}          
subsurface_properties[(2,3)]      = {'Psi':0.08, 'K':0.02, 'Delta_Theta':0.3, 'H':18.3} 

# (3,1):              
retention_basin_properties[(3,1)] = {'L':50.0, 'C_1':0.05, 'C_2':0.75, 'A_bmax':1320., 'Y_jmax':1.2, 't_dmin':12., 't_dmax':36}
watershed_properties[(3,1)]       = {'A_w': 5260.0, 'IDF_a': 114., 'IDF_b': 15.0/60., 'IDF_e':1.10, 'I_m':7.6*0.01, 'C':0.41}
subsurface_properties[(3,1)]      = {'Psi':0.13, 'K':0.028, 'Delta_Theta':0.3, 'H':18.3} 

# (3,2):
retention_basin_properties[(3,2)] = {'L':50.0, 'C_1':0.05, 'C_2':0.75, 'A_bmax':1720., 'Y_jmax':1.2, 't_dmin':12., 't_dmax':36}
watershed_properties[(3,2)]       = {'A_w': 6880.0, 'IDF_a': 114., 'IDF_b': 15.0/60., 'IDF_e':1.10, 'I_m':7.6*0.01, 'C':0.70} 
subsurface_properties[(3,2)]      = {'Psi':0.17, 'K':0.038, 'Delta_Theta':0.3, 'H':18.3}
 
# (4,1): 
retention_basin_properties[(4,1)] = {'L':60.0, 'C_1':0.05, 'C_2':0.75, 'A_bmax':1010., 'Y_jmax':1.2, 't_dmin':12., 't_dmax':36}
watershed_properties[(4,1)]       = {'A_w': 4050.0, 'IDF_a': 114., 'IDF_b': 15.0/60., 'IDF_e':1.10, 'I_m':7.6*0.01, 'C':0.32} 
subsurface_properties[(4,1)]      = {'Psi':0.14, 'K':0.023, 'Delta_Theta':0.3, 'H':18.3} 

