#! /usr/bin/env python
import subsurface_hydrology as gw
import surface_hydrology as sw
import retention_basin as db
import travis_mays_example as tmfun
import sys
# ------------------------------------------------------------------------------ #
# This is an alternate test example based on the Travis and Mays Samsi_13code .  #
# It will sereve as a test bed to learn how the pervious code worked and as a    # 
# place for possible extensions to be built.                                     #
# ============================================================================== # 
# The following block will be a space for alternate versions of the functions    #
# originally developed in the Travis and Mays example code.                      #
# ------------------------------------------------------------------------------ #

def test_optimization_basin1_test_example(infile=None,ntrial=10):
    # -------------------------------------------------------------------------- # 
    # Optimization driver function                                               #
    # -------------------------------------------------------------------------- #
    G = build_example_network(infile)
    G = tmfun.compute_basin_properties(G,display=True)
    tmfun.plot_network(G,'basin1_test_example_network')

    print "Running optimization on basin1_test_example -----"

    Network = tmfun.run_optimization_travis_mays(ntrial=ntrial,display_results=True)


def build_example_network_properties_AltExampleHardWire():
    """
    # ========================================================================= #
    # Build a retention network properties representation based on first example 
    # in Travis_Mays. These are all hard wired at the moment. 
    #
    # Here we use the (isodrainage line,index) convention:
    #
    #          (4,1)           : Travis and Mays Network
    #         /    \           : at left.  
    #     (3,2)  (3,1)         :
    #      /  \      \
    #  (2,3) (2,2)  (2,1)
    #                  \
    #                 (1,1) 
    # ========================================================================= #
    """

    # -------------------------------------------------------------------------- # 
    # Set up a blank dictionary for basin, watershed, and subsurface properties. #
    # -------------------------------------------------------------------------- #

    retention_basin_properties = {}; 
    watershed_properties       = {}; 
    subsurface_properties      = {};

    # -------------------------------------------------------------------------- # 
    # For each node in the network defind dictionary properties.                 #
    # -------------------------------------------------------------------------- #
    
    # (1,1):
    retention_basin_properties[(1,1)] = {'L':30.0,          # $/M^2 Cost of Land (per area)
                                         'C_1':0.05,        # $/M^3 Cost of Storage (per volume water)
                                         'C_2':0.75,        # Cost coefficeint
                                         'A_bmax':1010.,    # M^2 Max Area given site constraints
                                         'Y_jmax':1.2,      # M Max Ponding Depth based on 'regulation'
                                         't_dmin':12.,      # [h] minimum time to drain
                                         't_dmax':36}       # [h] maximum time to drain

    watershed_properties[(1,1)]       = {'A_w': 4050.0,     # M^2 Area of Watershed
                                         'IDF_a': 114.,     # Rainfall intensity - Duration- Frequency coefficient
                                         'IDF_b': 15.0/60., # [h] Rainfall intensity - Duration- Frequency coefficient
                                         'IDF_e':1.10,      # Rainfall intensity - Duration- Frequency coefficient
                                         'I_m':7.6*0.01,    # [m/h] Intensity of design storm 
                                         'C':0.51}          # [-] Runoff coefficient
                                   
    subsurface_properties[(1,1)]      = {'Psi':0.1,         # [m] Suction at infiltration front
                                         'K':0.03,          # [m/h] Hydraulic Conductivity
                                         'Delta_Theta':0.3, # difference in Saturated Volume Fraction and initial moisture content --> not specified in paper!
                                         'H':18.3}          # [m] Depth to water table
                                   
    # (2,1):
    retention_basin_properties[(2,1)] = {'L':40.0,          # $/M^2 Cost of Land (per area) 
                                         'C_1':0.05,        # $/M^3 Cost of Storage (per volume water)
                                         'C_2':0.75,        # Cost coefficeint
                                         'A_bmax':1110.,    # M^2 Max Area given site constraints
                                         'Y_jmax':1.2,      # M Max Ponding Depth based on 'regulation'
                                         't_dmin':12.,      # [h] minimum time to drain
                                         't_dmax':36}       # [h] maximum time to drain

    watershed_properties[(2,1)]       = {'A_w': 4450.0,     # M^2 Area of Watershed
                                         'IDF_a': 114.,     # Rainfall intensity - Duration- Frequency coefficient
                                         'IDF_b': 15.0/60., # [h] Rainfall intensity - Duration- Frequency coefficient
                                         'IDF_e':1.10,      # Rainfall intensity - Duration- Frequency coefficient
                                         'I_m':7.6*0.01,    # [m/h] Intensity of design storm 
                                         'C':0.62}          # [-] Runoff coefficient
                                   
    subsurface_properties[(2,1)]      = {'Psi':0.08,        # [m] Suction at infiltration front
                                         'K':0.02,          # [m/h] Hydraulic Conductivity
                                         'Delta_Theta':0.3, # difference in Saturated Volume Fraction and initial moisture content --> not specified in paper!
                                         'H':18.3}          # [m] Depth to water table
                                   
    # (2,2):
    retention_basin_properties[(2,2)] = {'L':30.0,          # $/M^2 Cost of Land (per area)
                                         'C_1':0.05,        # $/M^3 Cost of Storage (per volume water)
                                         'C_2':0.75,        # Cost coefficeint
                                         'A_bmax':2430.,    # M^2 Max Area given site constraints
                                         'Y_jmax':1.2,      # M Max Ponding Depth based on 'regulation'
                                         't_dmin':12.,      # [h] minimum time to drain
                                         't_dmax':36}       # [h] maximum time to drain

    watershed_properties[(2,2)]       = {'A_w': 9710.0,     # M^2 Area of Watershed
                                         'IDF_a': 114.,     # Rainfall intensity - Duration- Frequency coefficient
                                         'IDF_b': 15.0/60., # [h] Rainfall intensity - Duration- Frequency coefficient
                                         'IDF_e':1.10,      # Rainfall intensity - Duration- Frequency coefficient
                                         'I_m':7.6*0.01,    # [m/h] Intensity of design storm 
                                         'C':0.62}          # [-] Runoff coefficient
                                   
    subsurface_properties[(2,2)]      = {'Psi':0.09,        # [m] Suction at infiltration front
                                         'K':0.02,          # [m/h] Hydraulic Conductivity
                                         'Delta_Theta':0.3, # difference in Saturated Volume Fraction and initial moisture content --> not specified in paper!
                                         'H':18.3}          # [m] Depth to water table
           
    # (2,3):                        
    retention_basin_properties[(2,3)] = {'L':40.0,          # $/M^2 Cost of Land (per area)  
                                         'C_1':0.05,        # $/M^3 Cost of Storage (per volume water)
                                         'C_2':0.75,        # Cost coefficeint
                                         'A_bmax':1820.,    # M^2 Max Area given site constraints
                                         'Y_jmax':1.2,      # M Max Ponding Depth based on 'regulation'
                                         't_dmin':12.,      # [h] minimum time to drain
                                         't_dmax':36}       # [h] maximum time to drain

    watershed_properties[(2,3)]       = {'A_w': 7280.0,     # M^2 Area of Watershed
                                         'IDF_a': 114.,     # Rainfall intensity - Duration- Frequency coefficient
                                         'IDF_b': 15.0/60., # [h] Rainfall intensity - Duration- Frequency coefficient
                                         'IDF_e':1.10,      # Rainfall intensity - Duration- Frequency coefficient
                                         'I_m':7.6*0.01,    # [m/h] Intensity of design storm 
                                         'C':0.62}          # [-] Runoff coefficient
                                   
    subsurface_properties[(2,3)]      = {'Psi':0.08,        # [m] Suction at infiltration front
                                         'K':0.02,          # [m/h] Hydraulic Conductivity
                                         'Delta_Theta':0.3, # difference in Saturated Volume Fraction and initial moisture content --> not specified in paper!
                                         'H':18.3}          # [m] Depth to water table
                     
    # (3,1):              
    retention_basin_properties[(3,1)] = {'L':50.0,          # $/M^2 Cost of Land (per area)
                                         'C_1':0.05,        # $/M^3 Cost of Storage (per volume water)
                                         'C_2':0.75,        # Cost coefficeint
                                         'A_bmax':1320.,    # M^2 Max Area given site constraints
                                         'Y_jmax':1.2,      # M Max Ponding Depth based on 'regulation'
                                         't_dmin':12.,      # [h] minimum time to drain
                                         't_dmax':36}       # [h] maximum time to drain

    watershed_properties[(3,1)]       = {'A_w': 5260.0,     # M^2 Area of Watershed
                                         'IDF_a': 114.,     # Rainfall intensity - Duration- Frequency coefficient
                                         'IDF_b': 15.0/60., # [h] Rainfall intensity - Duration- Frequency coefficient
                                         'IDF_e':1.10,      # Rainfall intensity - Duration- Frequency coefficient
                                         'I_m':7.6*0.01,    # [m/h] Intensity of design storm 
                                         'C':0.41}          # [-] Runoff coefficient
                                   
    subsurface_properties[(3,1)]      = {'Psi':0.13,        # [m] Suction at infiltration front
                                         'K':0.028,         # [m/h] Hydraulic Conductivity
                                         'Delta_Theta':0.3, # difference in Saturated Volume Fraction and initial moisture content --> not specified in paper!
                                         'H':18.3}          # [m] Depth to water table
                                   
    # (3,2):
    retention_basin_properties[(3,2)] = {'L':50.0,          # $/M^2 Cost of Land (per area), mwf should be 50
                                         'C_1':0.05,        # $/M^3 Cost of Storage (per volume water)
                                         'C_2':0.75,        # Cost coefficeint
                                         'A_bmax':1720.,    # M^2 Max Area given site constraints
                                         'Y_jmax':1.2,      # M Max Ponding Depth based on 'regulation'
                                         't_dmin':12.,      # [h] minimum time to drain
                                         't_dmax':36}       # [h] maximum time to drain

    watershed_properties[(3,2)]       = {'A_w': 6880.0,     # M^2 Area of Watershed
                                         'IDF_a': 114.,     # Rainfall intensity - Duration- Frequency coefficient
                                         'IDF_b': 15.0/60., # [h] Rainfall intensity - Duration- Frequency coefficient
                                         'IDF_e':1.10,      # Rainfall intensity - Duration- Frequency coefficient
                                         'I_m':7.6*0.01,    # [m/h] Intensity of design storm 
                                         'C':0.70}          # [-] Runoff coefficient
                                   
    subsurface_properties[(3,2)]      = {'Psi':0.17,        # [m] Suction at infiltration front
                                         'K':0.038,         # [m/h] Hydraulic Conductivity
                                         'Delta_Theta':0.3, # difference in Saturated Volume Fraction and initial moisture content --> not specified in paper!
                                         'H':18.3}          # [m] Depth to water table
                                   
    # (4,1):
    retention_basin_properties[(4,1)] = {'L':60.0,          # $/M^2 Cost of Land (per area)
                                         'C_1':0.05,        # $/M^3 Cost of Storage (per volume water)
                                         'C_2':0.75,        # Cost coefficeint
                                         'A_bmax':1010.,    # M^2 Max Area given site constraints
                                         'Y_jmax':1.2,      # M Max Ponding Depth based on 'regulation'
                                         't_dmin':12.,      # [h] minimum time to drain
                                         't_dmax':36}       # [h] maximum time to drain

    watershed_properties[(4,1)]       = {'A_w': 4050.0,     # M^2 Area of Watershed
                                         'IDF_a': 114.,     # Rainfall intensity - Duration- Frequency coefficient
                                         'IDF_b': 15.0/60., # [h] Rainfall intensity - Duration- Frequency coefficient
                                         'IDF_e':1.10,      # Rainfall intensity - Duration- Frequency coefficient
                                         'I_m':7.6*0.01,    # [m/h] Intensity of design storm 
                                         'C':0.32}          # [-] Runoff coefficient
                                   
    subsurface_properties[(4,1)]      = {'Psi':0.14,        # [m] Suction at infiltration front
                                         'K':0.023,         # [m/h] Hydraulic Conductivity
                                         'Delta_Theta':0.3, # difference in Saturated Volume Fraction and initial moisture content --> not specified in paper!
                                         'H':18.3}          # [m] Depth to water table

    # -------------------------------------------------------------------------- # 
    # Return the basin, watershed, and subsurface property dictionaries.         #
    # -------------------------------------------------------------------------- # 

    return retention_basin_properties, watershed_properties, subsurface_properties

def build_example_network_topology_HardWire(plot=False,reverse_connectivity=True):

    """
    Build a retention network topology based on first example in Travis_Mays
    # ========================================================================= #
    # Build a retention network for the topology representation for the example 
    # in Travis_Mays. These are all hard wired this could/should be put 
    # together with the build_example_network_properties function above. . 
    #
    # Here we use the (isodrainage line,index) convention:
    #
    #          (4,1)           : Travis and Mays Network
    #         /    \           : at left.  
    #     (3,2)  (3,1)         :
    #      /  \      \
    #  (2,3) (2,2)  (2,1)
    #                  \
    #                 (1,1) 
    # ========================================================================= #
    """
    import networkx as nx

    # -------------------------------------------------------------------------- # 
    # Set the name for each of the nodes in the network set in build topology.   #
    # This will use the (isodrainage line,index) convention.                     #
    # -------------------------------------------------------------------------- #

    node_list = [(1,1),(2,1),(2,2),(2,3),(3,1),(3,2),(4,1)] 

    G = nx.DiGraph()

    G.add_nodes_from(node_list)

    # ========================================================================== #
    # Add network edges with the flow direction being upstream. This is the      #
    # reverse of parent child relationship in tree.                              #
    # ========================================================================== #

    G.add_edge((1,1),(2,1)); G.add_edge((2,1),(3,1)); G.add_edge((3,1),(4,1))
    G.add_edge((2,2),(3,2)); G.add_edge((2,3),(3,2)); G.add_edge((3,2),(4,1))

    # ========================================================================== #
    # If requested for easier for implementation reverse network connectivity    #
    # ========================================================================== #

    if reverse_connectivity: 
        G = G.reverse()
    if plot:
        tmfun.plot_network(G,'Travis_and_Mays_Hardwired_Example')
        
    return G

def build_example_network_topology(infile, G, plot=False,reverse_connectivity=True):

    """
    Build a retention network topology based on the network data given in a 
    specific python script. 
    # ========================================================================= #
    # Here we assume that the (isodrainage line,index) convention is used for 
    # all the nodes in the network. 
    # ========================================================================= #
    """

    # ========================================================================== #
    # If requested for easier for implementation reverse network connectivity    #
    # ========================================================================== #

    if reverse_connectivity: 
        G = G.reverse()
    if plot:
        tmfun.plot_network(G,str(infile))
        
    return G

def build_example_network(infile=None):
    """
    # Function builds a retention network representation. If 'infile=None' the hard wired example 
    # is that of the Travis and Mays paper. Else it will read from a given input script.     
    # 
    # When building a retention network with propoerties from a script defined as initial input 
    # the basin, watershed, and sub-watershed properties are all defined in the script. Note that 
    # retention_basin_properties, watershed_properties, and subsurface_properties need to be used 
    # as the dictionary definitions in the script. 
    #
    # The example in Travis_Mays is 'BasinConfig_TravisMays.py'. 
    """
    import networkx as nx
    if(infile == None):
        # Generates the hardwired example. 
        rtb_prop,wsh_prop,sub_prop = build_example_network_properties_AltExampleHardWire()
        G = build_example_network_topology_HardWire(plot=False,reverse_connectivity=True)
    else: 
        # Network parameters read from a python script.
        print "Reading from script: ", str(infile) 
        print "============================================"
        import importlib
        Basin = importlib.import_module(infile)
    
        rtb_prop = Basin.retention_basin_properties
        wsh_prop = Basin.watershed_properties
        sub_prop = Basin.subsurface_properties

        G = build_example_network_topology(infile,Basin.G,plot=False,reverse_connectivity=True)


    for key,val in rtb_prop.iteritems():
        G.node[key]['retention_basin_properties'] = val

    for key,val in wsh_prop.iteritems():
        G.node[key]['watershed_properties'] = val

    for key,val in sub_prop.iteritems():
        G.node[key]['subsurface_properties'] = val

    return G

# ------------------------------------------------------------------------------ #
# Simulation driver different from other examples.                               #
# ------------------------------------------------------------------------------ #

if __name__ == "__main__":

    """
    # ========================================================================= #
    # This is an updated simulation dirver with the goal of adding more 
    # flexability to the optimization networks used. 
    # 
    # Script Usage: 
    # Default      : ${PYTHON} basin1_test_example.py 
    # Defined input: ${PYTHON} basin1_test_example.py <BasinPropertisFile.py>
    # ========================================================================= #
    """

    # -------------------------------------------------------------------------- # 
    # If a command line argument is given properties will be read from there.    #
    # -------------------------------------------------------------------------- #

    if len(sys.argv) > 1:
        # Input from a file. 
        infile = str(sys.argv[1])
        print "============================================"
        print "Using defined inputfile: ",  str(infile) 
        print "============================================"
    else:
        # Default hardwired example. 
        infile = None
        print "============================================"
        print "Using hardwired Travis and Mays Example. 
        print "============================================"
   
    test_optimization_basin1_test_example(infile)

