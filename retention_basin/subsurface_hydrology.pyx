"""
Subsurface Hydrology approximations from Travis and Mays 08 Retention Basin optimization example
"""
import numpy
cimport numpy

cdef extern from "math.h":
    double log(double x)
#
ctypedef numpy.double_t DTYPE_t
#numpy.intc_t not in cython's numpy.pxd 
#ctypedef int ITYPE_t



def drainage_time(double Y,double Psi,double K,double delTheta):
    """
    Compute time to drain a ponded depth of Y based on Green-Ampt

    Y   -- Ponded depth [L]
    Psi -- Suction at wetting front [L]
    K   -- Saturated Hydraulic Conductivity [L/T]
    delTheta -- difference between saturated volume fraction and initial volume fraction

    assumes
      1 > delTheta > 0
      Psi >= 0
    """
    if Y <= 0.0:
        return 0.0
    assert 0. < delTheta and delTheta < 1.
    assert 0  <= Psi
    
    cdef double YdTh  = Y/delTheta
    cdef double Omega = (Psi + YdTh)/(Psi+Y)
    cdef double oneMdTh = 1.0-delTheta
    cdef double td = Y/(K*oneMdTh) - (Psi+Y)*delTheta/(K*oneMdTh*oneMdTh)*log(Omega)
    return td

def average_infiltration_rate(double Y,double Psi,double K,double delTheta):
    """
    Average infiltration rate for draining a ponded depth based on Green-Ampt

     f_a = F(t_d)/t_d = Y/t_d

    where
    F(t) -- the Green-Ampt infiltration depth
    t_d  -- time to drain ponded depth of Y
    Y   -- Ponded depth [L]
    Psi -- Suction at wetting front [L]
    K   -- Saturated Hydraulic Conductivity [L/T]
    delTheta -- difference between saturated volume fraction and initial volume fraction

    assumes
      1 > delTheta > 0
      Psi >= 0
    """
    if Y <= 0.0:
        return 0.0
    assert 0. < delTheta and delTheta < 1.
    assert 0  <= Psi
    
    cdef double YdTh  = Y/delTheta
    cdef double Omega = (Psi + YdTh)/(Psi+Y)
    cdef double oneMdTh = 1.0-delTheta
    cdef double lnOmega = log(Omega)
    cdef double f_a = (1. + oneMdTh*lnOmega/(Omega - 1.0 - lnOmega))*oneMdTh*K

    return f_a
    

def infiltration_rate_green_ampt(double Ft, double Y, double Psi, double K, double delTheta):
    """
    Infiltration rate from Green-Ampt assuming the infiltration depth has already been calculated

    F(t) -- the Green-Ampt infiltration depth

    t_d  -- time to drain ponded depth of Y
    Y   -- Ponded depth [L]
    Psi -- Suction at wetting front [L]
    K   -- Saturated Hydraulic Conductivity [L/T]
    delTheta -- difference between saturated volume fraction and initial volume fraction

 
    """
    cdef double drive = (Psi+Y)*delTheta/Ft + 1.0-delTheta
    cdef double ft = K*drive

    return ft

def infiltration_depth_green_ampt(double t, double Y, double Psi, double K, double delTheta):
    """
    Infiltration depth from Green-Ampt assuming the infiltration depth has already been calculated

    Basic definition is implicit in F(t).

    F(t) = (1-\Delta \theta)K t + [\frac{(\Psi+Y)\Delta \theta}{(1-\Delta \theta)}]
                                  \ln[1+\frac{(1-\Delta \theta)}{(\Psi + Y)\Delta \theta}]
    This is not correct yet

    F(t) -- the Green-Ampt infiltration depth

    t = time of infiltration [T]
    Y   -- Ponded depth [L]
    Psi -- Suction at wetting front [L]
    K   -- Saturated Hydraulic Conductivity [L/T]
    delTheta -- difference between saturated volume fraction and initial volume fraction

 
    """
    cdef double linear = (1.0-delTheta)*K*t
    cdef double Ft =  linear

    return Ft



