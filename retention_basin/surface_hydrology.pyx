"""
Surface Hydrology approximations from Travis and Mays 08 Retention Basin optimization example
"""
import numpy
cimport numpy

import subsurface_hydrology as gw

cdef extern from "math.h":
    double pow(double x, double y)
#

def peak_flow_rational(double eta, double C, double I_d, double A_w):
    """
    Peak flow calculation based on rational runoff equation 

    eta -- units conversion factor
    C   -- runoff coefficient (fraction of rainfall that results in runoff)
    A_w -- watershed area  [L^2]
    I_d -- rainfall intensity [L/T]
    """

    cdef Q_d = eta*C*I_d*A_w

    return Q_d

def rainfall_intensity(double t_m, double a, double b, double e):
    """
    Compute Rainfall intensity -- intensity for constant intensity storm of duration t_m with a specified return period 

    t_m  -- rainfall event duration, usually is time of concentration in rational method
            where time of concentration is the time for flow to reach the outlet from the farthest point in the watershed
            
    """
    cdef double Id = a/pow(t_m + b,e)

    return Id

def compute_design_storm_duration(double b, double e):
    """
    Simplified calculation of design storm duration assuming no infiltration into the basin during an event

    If e > 1.0 returns 0

    """
    if e <= 1.0:
        return 0.0
    cdef double t_m = b/(e-1.0)

    return t_m

def compute_storm_water_runoff_full(double t_m, double alpha, double beta, double I_m, double C, double A_w, double A,
                                    double Y, double Psi, double K, double delTheta):
    """
    Compute the storm water runoff from a basin allowing for infiltration in the retention basin

    where

    t_m    -- duration of rainfall event  [T]
    alpha,beta  -- unit conversion factors [-] 
    I_m    -- rainfall event intensity [L/T]
    C      -- runoff coefficient [-]
    A_w    -- watershed area [L^2]
    A      -- basin area [L^2]
    Y   -- Ponded depth [L]
    Psi -- Suction at wetting front [L]
    K   -- Saturated Hydraulic Conductivity [L/T]
    delTheta -- difference between saturated volume fraction and initial volume fraction

    """

    cdef double Ft = gw.infiltration_depth_green_ampt(t_m,Y,Psi,K,delTheta)
    cdef double peak_flow = peak_flow_rational(alpha,C,I_m,A_w)
    cdef double SRO = peak_flow*t_m - beta*A*Ft

    return SRO

def compute_storm_water_runoff(double t_m, double alpha, double I_m, double C, double A_w):
    """
    Compute the storm water runoff from a basin but ignore infiltration in the retention basin

    where

    t_m    -- duration of rainfall event  [T]
    alpha  -- unit conversion factor [-] 
    I_m    -- rainfall event intensity [L/T]
    C      -- runoff coefficient [-]
    A_w    -- watershed area [L^2]
    """

    cdef double peak_flow = peak_flow_rational(alpha,C,I_m,A_w)
    cdef double SRO = peak_flow*t_m 

    return SRO

