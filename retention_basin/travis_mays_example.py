#! /usr/bin/env python
import subsurface_hydrology as gw
import surface_hydrology as sw
import retention_basin as db
import sys
import numpy as np

def test_infil():
    K = 0.03  #m/h
    thetaS = 0.35 #[-]
    thetaI = 0.1  #[-]
    Psi    = 0.10 #[m]
    ponded_depth = 1.2 #[m]
    depth_to_water_table = 18.3 #[m]
    td = gw.drainage_time(ponded_depth,Psi,K,thetaS-thetaI)

    f_a= gw.average_infiltration_rate(ponded_depth,Psi,K,thetaS-thetaI)
    print """Soil properties:  Hydraulic Conductivity= %s [m/h] Saturated Moisture Content= %s [-] Initial Moisure Content= %s [-]
Front properties: Suction at front= %s [m]
Ponded Depth:  %s  [m] 
Time to Drain: %s  [h]
Average Infiltration Rate: %s [m/h]""" % (K,thetaS,thetaI,Psi,ponded_depth,td,f_a)

    A_hmax = db.maximum_basin_area_from_hydrology(depth_to_water_table,ponded_depth,Psi,K,thetaS-thetaI)

    print """Maximum allowed basin area from infiltration rate constraint: A_hmax= %s [m^2]
    """ % (A_hmax)

def test_runoff():
    #storm intensity parameters
    a = 114.0 #[-]
    b = 15.0 #[min]
    b *= 1.0/60. #[h]
    e = 1.10 # [-]
    t_m = sw.compute_design_storm_duration(b,e) #h

    #design intensity
    I_m = 0.076 # [m/h]
    
    #Watershed properties
    A_w = 4050.0 # [m^2]
    C   = 0.51 #[-]
    eta = 1.0
    Q_p = sw.peak_flow_rational(eta,C,I_m,A_w) #m^3/h
    SRO = sw.compute_storm_water_runoff(t_m,1.0,I_m,C,A_w) #m^3
    
    print """Storm intensity parameters:
       a= %s b= %s e= %s --> design storm duration, t_m= %s
Design storm intensity assumed I_m= %s
Watershed properties:
   Area: A_w= %s
   Runoff Coef: C= %s
   Peak flow: Q_p= %s
   Storm Water Runoff: SRO= %s
""" % (a,b,e,t_m,I_m,A_w,C,Q_p,SRO)

def maximum_basin_area_from_hydrology_v2(H,Y,Psi,K,delTheta):
    """
    Maximum allowed basin area based on requirement that subsurface seepage rate exceeds or
     matches infiltration rate from basin
    H   -- depth to water table [L]
    Y   -- Ponded depth [L]
    Psi -- Suction at wetting front [L]
    K   -- Saturated Hydraulic Conductivity [L/T]
    delTheta -- difference between saturated volume fraction and initial volume fraction

    
    """
    import math
    if Y <= 0.0:
        return 0.0
    assert 0. < delTheta and delTheta < 1.
    assert 0  <= Psi

    YdTh  = Y/delTheta
    Omega = (Psi + YdTh)/(Psi+Y)
    oneMdTh = 1.0-delTheta
    lnOmega = math.log(Omega)

    numer = 0.5*H*H/(oneMdTh*oneMdTh)*(2.0 - lnOmega/(Omega - 1. - delTheta*lnOmega))
    denom1 = oneMdTh*lnOmega/(Omega - 1. - lnOmega) - delTheta/oneMdTh
    denom = denom1*denom1

    return numer/denom
    
def build_example_network_properties_travis_and_mays():
    """
    Build a retention network properties representation based on first example in Travis_Mays. 
    These are all hard wired at the moment using this function call. Room to make more dynamic here. 
    
    """

    node_list = [(1,1),(2,1),(2,2),(2,3),(3,1),(3,2),(4,1)] #use (isodrainage line,index) convention
    #fill in input parameters
    retention_basin_properties = {} ; watershed_properties = {}; subsurface_properties = {}
    
    retention_basin_properties[(1,1)] = {'L':30.0, #$/M^2 Cost of Land (per area)
                                         'C_1':0.05,#$/M^3 Cost of Storage (per volume water)
                                         'C_2':0.75, #Cost coefficeint
                                         'A_bmax':1010.,#M^2 Max Area given site constraints
                                         'Y_jmax':1.2,  #M Max Ponding Depth based on 'regulation'
                                         't_dmin':12., #[h] minimum time to drain
                                         't_dmax':36} #[h] maximum time to drain

    watershed_properties[(1,1)] = {'A_w': 4050.0, #M^2 Area of Watershed
                                   'IDF_a': 114., #Rainfall intensity - Duration- Frequency coefficient
                                   'IDF_b': 15.0/60., #[h] Rainfall intensity - Duration- Frequency coefficient
                                   'IDF_e':1.10, #Rainfall intensity - Duration- Frequency coefficient
                                   'I_m':7.6*0.01, #[m/h] Intensity of design storm 
                                   'C':0.51} #[-] Runoff coefficient
                                   
    subsurface_properties[(1,1)]= {'Psi':0.1, #[m] Suction at infiltration front
                                   'K':0.03,  #[m/h] Hydraulic Conductivity
                                   'Delta_Theta':0.3, #difference in Saturated Volume Fraction and initial moisture content --> not specified in paper!
                                   'H':18.3} #[m] Depth to water table
                                   

    #fill in input parameters 
    retention_basin_properties[(2,1)] = {'L':40.0, #$/M^2 Cost of Land (per area) 
                                         'C_1':0.05,#$/M^3 Cost of Storage (per volume water)
                                         'C_2':0.75, #Cost coefficeint
                                         'A_bmax':1110.,#M^2 Max Area given site constraints
                                         'Y_jmax':1.2,  #M Max Ponding Depth based on 'regulation'
                                         't_dmin':12., #[h] minimum time to drain
                                         't_dmax':36} #[h] maximum time to drain

    watershed_properties[(2,1)] = {'A_w': 4450.0, #M^2 Area of Watershed
                                   'IDF_a': 114., #Rainfall intensity - Duration- Frequency coefficient
                                   'IDF_b': 15.0/60., #[h] Rainfall intensity - Duration- Frequency coefficient
                                   'IDF_e':1.10, #Rainfall intensity - Duration- Frequency coefficient
                                   'I_m':7.6*0.01, #[m/h] Intensity of design storm 
                                   'C':0.62} #[-] Runoff coefficient
                                   
    subsurface_properties[(2,1)]= {'Psi':0.08, #[m] Suction at infiltration front
                                   'K':0.02,  #[m/h] Hydraulic Conductivity
                                   'Delta_Theta':0.3, #difference in Saturated Volume Fraction and initial moisture content --> not specified in paper!
                                   'H':18.3} #[m] Depth to water table
                                   
    retention_basin_properties[(2,2)] = {'L':30.0, #$/M^2 Cost of Land (per area)
                                         'C_1':0.05,#$/M^3 Cost of Storage (per volume water)
                                         'C_2':0.75, #Cost coefficeint
                                         'A_bmax':2430.,#M^2 Max Area given site constraints
                                         'Y_jmax':1.2,  #M Max Ponding Depth based on 'regulation'
                                         't_dmin':12., #[h] minimum time to drain
                                         't_dmax':36} #[h] maximum time to drain

    watershed_properties[(2,2)] = {'A_w': 9710.0, #M^2 Area of Watershed
                                   'IDF_a': 114., #Rainfall intensity - Duration- Frequency coefficient
                                   'IDF_b': 15.0/60., #[h] Rainfall intensity - Duration- Frequency coefficient
                                   'IDF_e':1.10, #Rainfall intensity - Duration- Frequency coefficient
                                   'I_m':7.6*0.01, #[m/h] Intensity of design storm 
                                   'C':0.62} #[-] Runoff coefficient
                                   
    subsurface_properties[(2,2)]= {'Psi':0.09, #[m] Suction at infiltration front
                                   'K':0.02,  #[m/h] Hydraulic Conductivity
                                   'Delta_Theta':0.3, #difference in Saturated Volume Fraction and initial moisture content --> not specified in paper!
                                   'H':18.3} #[m] Depth to water table
                                   
    retention_basin_properties[(2,3)] = {'L':40.0, #$/M^2 Cost of Land (per area)  
                                         'C_1':0.05,#$/M^3 Cost of Storage (per volume water)
                                         'C_2':0.75, #Cost coefficeint
                                         'A_bmax':1820.,#M^2 Max Area given site constraints
                                         'Y_jmax':1.2,  #M Max Ponding Depth based on 'regulation'
                                         't_dmin':12., #[h] minimum time to drain
                                         't_dmax':36} #[h] maximum time to drain

    watershed_properties[(2,3)] = {'A_w': 7280.0, #M^2 Area of Watershed
                                   'IDF_a': 114., #Rainfall intensity - Duration- Frequency coefficient
                                   'IDF_b': 15.0/60., #[h] Rainfall intensity - Duration- Frequency coefficient
                                   'IDF_e':1.10, #Rainfall intensity - Duration- Frequency coefficient
                                   'I_m':7.6*0.01, #[m/h] Intensity of design storm 
                                   'C':0.62} #[-] Runoff coefficient
                                   
    subsurface_properties[(2,3)]= {'Psi':0.08, #[m] Suction at infiltration front
                                   'K':0.02,  #[m/h] Hydraulic Conductivity
                                   'Delta_Theta':0.3, #difference in Saturated Volume Fraction and initial moisture content --> not specified in paper!
                                   'H':18.3} #[m] Depth to water table
                                   
    retention_basin_properties[(3,1)] = {'L':50.0, #$/M^2 Cost of Land (per area)
                                         'C_1':0.05,#$/M^3 Cost of Storage (per volume water)
                                         'C_2':0.75, #Cost coefficeint
                                         'A_bmax':1320.,#M^2 Max Area given site constraints
                                         'Y_jmax':1.2,  #M Max Ponding Depth based on 'regulation'
                                         't_dmin':12., #[h] minimum time to drain
                                         't_dmax':36} #[h] maximum time to drain

    watershed_properties[(3,1)] = {'A_w': 5260.0, #M^2 Area of Watershed
                                   'IDF_a': 114., #Rainfall intensity - Duration- Frequency coefficient
                                   'IDF_b': 15.0/60., #[h] Rainfall intensity - Duration- Frequency coefficient
                                   'IDF_e':1.10, #Rainfall intensity - Duration- Frequency coefficient
                                   'I_m':7.6*0.01, #[m/h] Intensity of design storm 
                                   'C':0.41} #[-] Runoff coefficient
                                   
    subsurface_properties[(3,1)]= {'Psi':0.13, #[m] Suction at infiltration front
                                   'K':0.028,  #[m/h] Hydraulic Conductivity
                                   'Delta_Theta':0.3, #difference in Saturated Volume Fraction and initial moisture content --> not specified in paper!
                                   'H':18.3} #[m] Depth to water table
                                   
    retention_basin_properties[(3,2)] = {'L':50.0, #$/M^2 Cost of Land (per area), 
                                         'C_1':0.05,#$/M^3 Cost of Storage (per volume water)
                                         'C_2':0.75, #Cost coefficeint
                                         'A_bmax':1720.,#M^2 Max Area given site constraints
                                         'Y_jmax':1.2,  #M Max Ponding Depth based on 'regulation'
                                         't_dmin':12., #[h] minimum time to drain
                                         't_dmax':36} #[h] maximum time to drain

    watershed_properties[(3,2)] = {'A_w': 6880.0, #M^2 Area of Watershed
                                   'IDF_a': 114., #Rainfall intensity - Duration- Frequency coefficient
                                   'IDF_b': 15.0/60., #[h] Rainfall intensity - Duration- Frequency coefficient
                                   'IDF_e':1.10, #Rainfall intensity - Duration- Frequency coefficient
                                   'I_m':7.6*0.01, #[m/h] Intensity of design storm 
                                   'C':0.70} #[-] Runoff coefficient
                                   
    subsurface_properties[(3,2)]= {'Psi':0.17, #[m] Suction at infiltration front
                                   'K':0.038,  #[m/h] Hydraulic Conductivity
                                   'Delta_Theta':0.3, #difference in Saturated Volume Fraction and initial moisture content --> not specified in paper!
                                   'H':18.3} #[m] Depth to water table
                                   
    retention_basin_properties[(4,1)] = {'L':6.0, #$/M^2 Cost of Land (per area) #mwf debug orig 'L':60.0, #$/M^2 Cost of Land (per area)
                                         'C_1':0.05,#$/M^3 Cost of Storage (per volume water)
                                         'C_2':0.75, #Cost coefficeint
                                         'A_bmax':1010.,#M^2 Max Area given site constraints
                                         'Y_jmax':1.2,  #M Max Ponding Depth based on 'regulation'
                                         't_dmin':12., #[h] minimum time to drain
                                         't_dmax':36} #[h] maximum time to drain

    watershed_properties[(4,1)] = {'A_w': 4050.0, #M^2 Area of Watershed
                                   'IDF_a': 114., #Rainfall intensity - Duration- Frequency coefficient
                                   'IDF_b': 15.0/60., #[h] Rainfall intensity - Duration- Frequency coefficient
                                   'IDF_e':1.10, #Rainfall intensity - Duration- Frequency coefficient
                                   'I_m':7.6*0.01, #[m/h] Intensity of design storm 
                                   'C':0.32} #[-] Runoff coefficient
                                   
    subsurface_properties[(4,1)]= {'Psi':0.14, #[m] Suction at infiltration front
                                   'K':0.023,  #[m/h] Hydraulic Conductivity
                                   'Delta_Theta':0.3, #difference in Saturated Volume Fraction and initial moisture content --> not specified in paper!
                                   'H':18.3} #[m] Depth to water table


    return retention_basin_properties,watershed_properties,subsurface_properties


def build_example_network_properties():
    """
    Build a retention network properties representation based on first example in Travis_Mays. 
    This reads in data from a given file, and populates the same old Travis and Mays network with different network properties. 
    
    """

    f=open('input','r')
    node_list = [(1,1),(2,1),(2,2),(2,3),(3,1),(3,2),(4,1)] #use (isodrainage line,index) convention
    #fill in input parameters
    retention_basin_properties = {} ; watershed_properties = {}; subsurface_properties = {}
    
    i=0
    retention_basin_properties={};watershed_properties={};subsurface_properties={}
    for line in f:
        #ignore lines that begin with #
        if line[0]!='#':
          bi=[float(x.strip()) for x in line.split(',')]
          retention_basin_properties[node_list[i]]={'L':bi[0],'C_1':bi[1],'C_2':bi[2],'A_bmax':bi[3],'Y_jmax':bi[4],'t_dmin':bi[5],'t_dmax':bi[6]}
#          retention_basin_properties[node_list[i]]={'L':L[i],'C_1':bi[1],'C_2':bi[2],'A_bmax':bi[3],'Y_jmax':bi[4],'t_dmin':bi[5],'t_dmax':bi[6]}
          watershed_properties[node_list[i]]={'A_w':bi[7],'IDF_a':bi[8],'IDF_b':bi[9],'IDF_e':bi[10],'I_m':bi[11],'C':bi[12]}
          subsurface_properties[node_list[i]]={'Psi':bi[13],'K':bi[14],'Delta_Theta':bi[15],'H':bi[16]}
          i=i+1

    return retention_basin_properties,watershed_properties,subsurface_properties

def build_example_network_topology(plot=False,reverse_connectivity=True):
    """
    Build a retention network topology based on first example in Travis_Mays
    """
    import networkx as nx

    node_list = [(1,1),(2,1),(2,2),(2,3),(3,1),(3,2),(4,1)] #use (isodrainage line,index) convention
    G = nx.DiGraph()
    G.add_nodes_from(node_list)
    #flow direction, reverse of parent child relationship in tree
    #G.add_edges_from([((1,1),(2,1)),((2,1),(3,1)),((3,1),(4,1)),((2,2),(3,2)),((2,3),(3,2)),((3,2),(4,1))])
    G.add_edge((1,1),(2,1)); G.add_edge((2,1),(3,1)); G.add_edge((3,1),(4,1))
    G.add_edge((2,2),(3,2)); G.add_edge((2,3),(3,2)); G.add_edge((3,2),(4,1))
    if reverse_connectivity: #easier for algorithm implementation
        G = G.reverse()
    if plot:
        plot_network(G,'example1')
        
    return G

def build_pajaro_network_topology(plot=True,reverse_connectivity=True):
    """
    Build a retention network topology based on pajaro subwatershed network. 
    """
    import networkx as nx

    node_list = [1,2,3,4,5,6,7] #use (isodrainage line,index) convention
    G = nx.DiGraph()
    G.add_nodes_from(node_list)
    #flow direction, reverse of parent child relationship in tree
    G.add_edge(1,2); G.add_edge(1,3);
    G.add_edge(2,5)
    G.add_edge(3,4)
    G.add_edge(5,6)
    G.add_edge(6,7)
    if reverse_connectivity: #easier for algorithm implementation
        G = G.reverse()
        #for e in G.edges():
        #    G.remove_edge(e[0],e[1])
        #    G.add_edge(e[1],e[0])
    
    if plot:
        plot_network(G,'pajaro_network')
        
    return G


def build_example_network(use_travis_and_mays_props=True):
    """
    Build a retention network representation based on first example in Travis_Mays
    """
    import networkx as nx
    if use_travis_and_mays_props:
        rtb_prop,wsh_prop,sub_prop = build_example_network_properties_travis_and_mays()
    else: #read from a file
        rtb_prop,wsh_prop,sub_prop = build_example_network_properties()

    #for now still hardwired for Travis and Mays topology
    G =  build_example_network_topology()

    for key,val in rtb_prop.iteritems():
        G.node[key]['retention_basin_properties'] = val

    for key,val in wsh_prop.iteritems():
        G.node[key]['watershed_properties'] = val

    for key,val in sub_prop.iteritems():
        G.node[key]['subsurface_properties'] = val

    return G

def plot_network(G,filebase,node_labels=None):
    import networkx as nx
    import matplotlib.pyplot as plt
    nx.write_dot(G,filebase+'.dot')

    # same layout using matplotlib with no labels
    plt.title("network for %s" % filebase)
    pos=nx.graphviz_layout(G,prog='dot')
    nx.draw(G,pos,node_size=550,node_shape='s',with_labels=True,arrows=True,labels=node_labels)
    plt.show()
    plt.savefig(filebase+'.png')
    plt.clf()
    

def compute_basin_properties(G,display=False):
    """
    Compute basin properties for a given network G using specified input properties

    Retention-Basin (local) Properties
      Y_dmax : maximum ponding depth based on maximum allowed drainage time
      Y_dmin : minimum ponding depth based on minimum allowed drainage time
      A_hmax : maximum allowed basin area based on hydrologic conditions
      
    Subsurface Related Properties
      f_a    : average infiltration rate for ponded depth Y_dmax
    Watershed Related Properties
      t_m    : design storm duration
      SRO    : stormwater Runoff

    """
    
    import scipy.optimize as opt

    for basin in G.nodes():
        #--- Subsurface Properties ---
        #Y_dmax is such that t_d(Y_dmax) = t_dmax
        def f(Y,Psi,K,delTheta):
            return G.node[basin]['retention_basin_properties']['t_dmax']-gw.drainage_time(Y,Psi,K,delTheta) 
        Y0 = 0.5*G.node[basin]['retention_basin_properties']['Y_jmax']; tol =1.5e-8
        Y_dmax = opt.newton(f,Y0,args=(G.node[basin]['subsurface_properties']['Psi'],
                                       G.node[basin]['subsurface_properties']['K'],
                                       G.node[basin]['subsurface_properties']['Delta_Theta']),
                            tol=tol,maxiter=50)
        #test convergence, appears that doesn't return much information
        residual = f(Y_dmax,G.node[basin]['subsurface_properties']['Psi'],
                     G.node[basin]['subsurface_properties']['K'],
                     G.node[basin]['subsurface_properties']['Delta_Theta'])
        if abs(residual) > tol:
            print "Warning! Newton solve for Y_dmax did not meet residual criterion: residual(%s)= %s, tolerance= %s " % (Y_dmax,residual,tol)
        G.node[basin]['retention_basin_properties']['Y_dmax']=Y_dmax

        #enforce bounds constraint on maximum allowed depth min of hydrology and building constraints
        G.node[basin]['retention_basin_properties']['Y_max'] = min(G.node[basin]['retention_basin_properties']['Y_dmax'],
                                                                   G.node[basin]['retention_basin_properties']['Y_jmax'])
        #compute average infiltration rate for Y_max
        G.node[basin]['subsurface_properties']['f_a'] = gw.average_infiltration_rate(G.node[basin]['retention_basin_properties']['Y_max'],
                                                                                     G.node[basin]['subsurface_properties']['Psi'],
                                                                                     G.node[basin]['subsurface_properties']['K'],
                                                                                     G.node[basin]['subsurface_properties']['Delta_Theta'])


        #Y_dmin is such that t_d(Y_dmin) = t_dmin
        def f(Y,Psi,K,delTheta):
            return G.node[basin]['retention_basin_properties']['t_dmin']-gw.drainage_time(Y,Psi,K,delTheta) 
        Y0 = 0.5*G.node[basin]['retention_basin_properties']['Y_jmax']; tol =1.5e-8
        Y_min = opt.newton(f,Y0,args=(G.node[basin]['subsurface_properties']['Psi'],
                                      G.node[basin]['subsurface_properties']['K'],
                                      G.node[basin]['subsurface_properties']['Delta_Theta']),
                            tol=tol,maxiter=50)
        #test convergence, appears that doesn't return much information
        residual = f(Y_min,G.node[basin]['subsurface_properties']['Psi'],
                     G.node[basin]['subsurface_properties']['K'],
                     G.node[basin]['subsurface_properties']['Delta_Theta'])
        if abs(residual) > tol:
            print "Warning! Newton solve for Y_dmin did not meet residual criterion: residual(%s)= %s, tolerance= %s " % (Y_min,residual,tol)
        G.node[basin]['retention_basin_properties']['Y_min']=Y_min

        G.node[basin]['retention_basin_properties']['A_hmax']=db.maximum_basin_area_from_hydrology(G.node[basin]['subsurface_properties']['H'],
                                                                                                   G.node[basin]['retention_basin_properties']['Y_max'],
                                                                                                   G.node[basin]['subsurface_properties']['Psi'],
                                                                                                   G.node[basin]['subsurface_properties']['K'],
                                                                                                   G.node[basin]['subsurface_properties']['Delta_Theta'])
        #enforce constraint on maximum area: min of hydrologically allowed and construction bound constraint
        G.node[basin]['retention_basin_properties']['A_max']=min(G.node[basin]['retention_basin_properties']['A_hmax'],
                                                                 G.node[basin]['retention_basin_properties']['A_bmax'])

        #
        G.node[basin]['retention_basin_properties']['S_pmax']=G.node[basin]['retention_basin_properties']['A_max']*G.node[basin]['retention_basin_properties']['Y_max']
        
        #--- Watershed Properties ---
        G.node[basin]['watershed_properties']['t_m']= sw.compute_design_storm_duration(G.node[basin]['watershed_properties']['IDF_b'],
                                                                                       G.node[basin]['watershed_properties']['IDF_e'])
        G.node[basin]['watershed_properties']['SRO']= sw.compute_storm_water_runoff(G.node[basin]['watershed_properties']['t_m'],
                                                                                    1.0,#no units conversion needed here
                                                                                    G.node[basin]['watershed_properties']['I_m'],
                                                                                    G.node[basin]['watershed_properties']['C'],
                                                                                    G.node[basin]['watershed_properties']['A_w'])
        
    #end loop
    for basin in sorted(G.nodes()):
        output = """{s:*>10} Basin {basin} {s:*<10}
--------------------------
Retention Basin Properties:
--------------------------
Y_min = {Y_min:10.3f}
Y_dmax= {Y_dmax:10.3f}
Y_max = {Y_max:10.3f}
A_hmax= {A_hmax:10.3f}
A_max = {A_max:10.3f}
S_pmax= {S_pmax:10.3f}
--------------------------
Subsurface Properties:
--------------------------
f_a   = {f_a:10.3f}

--------------------------
Watershed Properties:
--------------------------
t_m  = {t_m:10.3f}
SRO  = {SRO:10.3f}
    """.format(s='',
    basin=basin,
    Y_min = G.node[basin]['retention_basin_properties']['Y_min'],
    Y_dmax= G.node[basin]['retention_basin_properties']['Y_dmax'],
    Y_max = G.node[basin]['retention_basin_properties']['Y_max'],
    A_hmax= G.node[basin]['retention_basin_properties']['A_hmax'],
    A_max = G.node[basin]['retention_basin_properties']['A_max'],
    S_pmax= G.node[basin]['retention_basin_properties']['S_pmax'],
    f_a   = G.node[basin]['subsurface_properties']['f_a'],
    t_m   = G.node[basin]['watershed_properties']['t_m'],
    SRO   = G.node[basin]['watershed_properties']['SRO'],
    )
        if display is True:
         print output
    return G

def compute_total_runoff(G,basin):
    """
    Visit nodes in graph and compute TRO as

                  SRO_{1,i}        n = n_{min,i} --> leaf node
    TRO_{n,i} =
                  SRO_{n,i}+\sum_{i'=i_{s,n,i}}{i'=i_{f,n,i}} TRO_{n-1,i'}  otherwise

    Here \sum_{i'=i_{s,n,i}}{i'=i_{f,n,i}} means sum over children 
                  
    """
    G.node[basin]['TRO'] = G.node[basin]['watershed_properties']['SRO']
    if len(G.out_edges(basin)) == 0:
        return G
    else:
        for edge in G.out_edges(basin):
            child = edge[1]
            G = compute_total_runoff(G,child)
            G.node[basin]['TRO'] += G.node[child]['TRO']
        return G

def compute_total_storage(G,basin):
    """
    Compute total storage at basin, T_{n,i},
       assuming a value of the local storage, S_{n-1,i} at all nodes
       has been chosen and the total runoff at the node has been computed


                  S_{1,i}        n = n_{min,i} --> leaf node
    T_{n,i} =
                  S_{n,i}+\sum_{i'=i_{s,n,i}}{i'=i_{f,n,i}} T_{n-1,i'}  otherwise

    Here \sum_{i'=i_{s,n,i}}{i'=i_{f,n,i}} means sum over children 

    Does not check bounds on S
    """
    assert G.node[basin].has_key('S')
    assert G.node[basin].has_key('TRO')

    G.node[basin]['T'] = G.node[basin]['S']
    if len(G.out_edges(basin)) == 0:
        #enforce constraint on TRO
        G.node[basin]['T'] = min(G.node[basin]['S'],G.node[basin]['TRO'])
        return G
    else:
        for edge in G.out_edges(basin):
            child = edge[1]
            G = compute_total_storage(G,child)
            G.node[basin]['T'] += G.node[child]['T']
        #enforce constraint on TRO
        G.node[basin]['T'] = min(G.node[basin]['S'],G.node[basin]['TRO'])
        return G


def optimize_storage_iteration(G,basin,ntrial=10,is_root=False):
    """
    Try to implement Travis and May's algorithm for optimal storage basin sizes in network

    Pick K_{n,i} possible local storage values at each basin
       S_{n,i,k} \in [0,TRO_{n,i}]

    For each basin (n,i):
    
        If (n,i) has no children then solve
        
            f^{*}_{n,i} =  min_{k}[ R_{n,i}(S_{n,i,k}) ]

        Otherwise, solve
        
            f^{*}_{n,i} =  min_{k}[ R_{n,i}(S_{n,i,k}) + \sum_{i'=i_{s,n,i}}{i'=i_{f,n,i}} f^{*}_{n-1,i}]

    I'm going to interpret the first step as just
      setting R^{*}_{n,i} = R(S^*_{n,i,k}) for a particular k where

         S^*_{n,i,k} = max[0,min[S_{n,i,k},TRO_{n,i}, S_{p,max,n,i}]]
           
    For basin's with an upstream contribution, for each iterate k \in K_{n,i}
       we first compute the children's total storage for k, then pick an optimum     
    """

    assert G.node[basin].has_key('TRO')
    import numpy as np
    import sys
    ntrial_local = ntrial
    #possible total storage values for this node
    TRO_nik = np.linspace(0.,G.node[basin]['TRO'],ntrial)
    if is_root:
        TRO_nik.fill(G.node[basin]['TRO'])
    #for each trial option    
    G.node[basin]['S_k'] = np.zeros((ntrial,),'d')
    G.node[basin]['A_k'] = np.zeros((ntrial,),'d')
    G.node[basin]['T_k'] = np.zeros((ntrial,),'d')
    G.node[basin]['R_k'] = np.zeros((ntrial,),'d')
    G.node[basin]['f_k'] = np.zeros((ntrial,),'d')

    if len(G.out_edges(basin)) == 0:
        #enforce constraint on TRO
        G.node[basin]['S_k'] = np.minimum(TRO_nik,G.node[basin]['retention_basin_properties']['S_pmax'])
        if G.node[basin]['retention_basin_properties']['S_pmax'] > 0.:
            G.node[basin]['A_k'] = G.node[basin]['S_k']/G.node[basin]['retention_basin_properties']['Y_max']
        G.node[basin]['T_k'] = G.node[basin]['S_k'].copy()
        G.node[basin]['R_k'] = G.node[basin]['retention_basin_properties']['L']*G.node[basin]['A_k'] + G.node[basin]['retention_basin_properties']['C_1']*np.power(G.node[basin]['S_k'],G.node[basin]['retention_basin_properties']['C_2'])
        G.node[basin]['f_k'] = G.node[basin]['R_k']
        G.node[basin]['upstream_optimal_k'] = None
        return G
    else:
        children = G.neighbors(basin) #Directed Graph, with children as out-edges
        for child in children: #loop over children and compute costs
            #should compute T_k, R_k, S_k, A_k, f_k, possibly optimal upstream storage options (upstream_optimal_k) for each ntrial options
            G = optimize_storage_iteration(G,child,ntrial=ntrial)
            for key in ['S_k','T_k','R_k','S_k','A_k','f_k','upstream_optimal_k']:
                assert G.node[child].has_key(key), "could not find key = %s " % key
        #end first child step
        nchildren = len(children)
        #just try to get something working
        assert nchildren <= 2, "only testing for 1 or 2 children right now"
        #define all possible combinations of upstream storages ...
        table_dim = [ntrial]*nchildren
        #T_nm1ik[l,m] = T_{n-1,0,l} + T_{n-1,1,m} 
        G.node[basin]['T_nm1ik'] = np.zeros(tuple(table_dim),'d') 
        G.node[basin]['f_nm1ik'] = np.zeros(tuple(table_dim),'d')
        for k in range(ntrial):
            G.node[basin]['T_nm1ik'][k,...] = G.node[children[0]]['T_k'][k]
            G.node[basin]['f_nm1ik'][k,...] = G.node[children[0]]['f_k'][k]
        if nchildren > 1:
            for k in range(ntrial):
                G.node[basin]['T_nm1ik'][...,k] += G.node[children[1]]['T_k'][k]
                G.node[basin]['f_nm1ik'][...,k] += G.node[children[1]]['f_k'][k]
        #

        #for each candidate total storage at this node compute
        # local storage needed for each combination of upstream runoff
        # cost for each combination of local storage and upstream runoff
        # pick minimum one and save value and cost in arrays T_k, R_k, f_k
        # record the optimal upstream contribution for that k in ['upstream_optimal_k'][child][k]
        #
        #optimal upstream contributions for each k
        G.node[basin]['upstream_optimal_k'] = {}
        for m in range(nchildren):
            G.node[basin]['upstream_optimal_k'][children[m]] = {}
        #
        for k in range(ntrial):                    
            #local storage needed for this candidate runoff  
            S_ik = np.minimum(TRO_nik[k] - G.node[basin]['T_nm1ik'],G.node[basin]['retention_basin_properties']['S_pmax'])
            S_ik = np.where(S_ik < 0.0, 0.0, S_ik)
            T_ik = S_ik + G.node[basin]['T_nm1ik']
            #area associated with the storage
            if G.node[basin]['retention_basin_properties']['S_pmax'] > 0.:
                A_ik = S_ik/G.node[basin]['retention_basin_properties']['Y_max']
            else:
                A_ik = np.zeros(S_ik.shape,'d')
            #cost of the local storage
            R_ik = G.node[basin]['retention_basin_properties']['L']*A_ik + G.node[basin]['retention_basin_properties']['C_1']*np.power(S_ik,G.node[basin]['retention_basin_properties']['C_2'])
            f_ik = R_ik + G.node[basin]['f_nm1ik']

            if is_root: #enforce constraint on total outflow
                f_ik = np.where(T_ik < TRO_nik[k], sys.float_info.max, f_ik)
            min_index_flat = np.argmin(f_ik);
            min_index = np.unravel_index(min_index_flat,f_ik.shape)
            G.node[basin]['S_k'][k] = S_ik[min_index]
            G.node[basin]['A_k'][k] = A_ik[min_index]
            G.node[basin]['T_k'][k] = S_ik[min_index] + G.node[basin]['T_nm1ik'][min_index]
            G.node[basin]['R_k'][k] = R_ik[min_index]
            G.node[basin]['f_k'][k] = f_ik[min_index]
            for m in range(len(min_index)):
                G.node[basin]['upstream_optimal_k'][children[m]][k] = min_index[m]
        #end loop through different trial options

        return G
def travis_and_mays_cost_function(A,S,retention_basin_properties,watershed_properties,subsurface_properties):
    """
    Original Travis and Mays Cost Function

    R = L*A + C_1 S^C_2

    """
    return retention_basin_properties['L']*A + retention_basin_properties['C_1']*np.power(S,retention_basin_properties['C_2'])

def network_recharge_cost_function(A,S,T,retention_basin_properties,watershed_properties,subsurface_properties):
    """
    Measure the total amount of recharge captured over the network
    R_0 = \alpha_0 |(T-Q_0)|
    """
    return watershed_properties['recharge_weight']*np.absolute(T-watershed_properties['total_recharge_target'])

def optimize_storage_iteration_user_cost(G,basin,cost_function,ntrial=10,is_root=False,enforce_total_runoff_constraint=True,
                                         network_cost_function=None):
    """
    Try to implement Travis and May's algorithm for optimal storage basin sizes in network

    Pick K_{n,i} possible local storage values at each basin
       S_{n,i,k} \in [0,TRO_{n,i}]

    For each basin (n,i):
    
        If (n,i) has no children then solve
        
            f^{*}_{n,i} =  min_{k}[ R_{n,i}(S_{n,i,k}) ]

        Otherwise, solve
        
            f^{*}_{n,i} =  min_{k}[ R_{n,i}(S_{n,i,k}) + \sum_{i'=i_{s,n,i}}{i'=i_{f,n,i}} f^{*}_{n-1,i}]

    I'm going to interpret the first step as just
      setting R^{*}_{n,i} = R(S^*_{n,i,k}) for a particular k where

         S^*_{n,i,k} = max[0,min[S_{n,i,k},TRO_{n,i}, S_{p,max,n,i}]]
           
    For basin's with an upstream contribution, for each iterate k \in K_{n,i}
       we first compute the children's total storage for k, then pick an optimum     

    ---------------------------------------------------------------------------------------------------------
    This version allows specification of the cost function to be minimized through the cost_function variable

    R_k = cost_function(A_k,S_k,retention_props,watershed_props,subsurface_props)

    and a possible 'network-scale' cost function that is applied at the root node. This is meant to be a 
     way to make the cost be
    total_cost = R_k + |\alpha_0*(T-Q_0)| 

    This is specified through the network_cost_function argument
    """

    assert G.node[basin].has_key('TRO')
    import numpy as np
    import sys
    ntrial_local = ntrial
    #possible total storage values for this node
    TRO_nik = np.linspace(0.,G.node[basin]['TRO'],ntrial)
    if is_root and enforce_total_runoff_constraint:
        TRO_nik.fill(G.node[basin]['TRO'])
    #for each trial option    
    G.node[basin]['S_k'] = np.zeros((ntrial,),'d')
    G.node[basin]['A_k'] = np.zeros((ntrial,),'d')
    G.node[basin]['T_k'] = np.zeros((ntrial,),'d')
    G.node[basin]['R_k'] = np.zeros((ntrial,),'d')
    G.node[basin]['f_k'] = np.zeros((ntrial,),'d')

    if len(G.out_edges(basin)) == 0:
        #enforce constraint on TRO
        G.node[basin]['S_k'] = np.minimum(TRO_nik,G.node[basin]['retention_basin_properties']['S_pmax'])
        if G.node[basin]['retention_basin_properties']['S_pmax'] > 0.:
            G.node[basin]['A_k'] = G.node[basin]['S_k']/G.node[basin]['retention_basin_properties']['Y_max']
        G.node[basin]['T_k'] = G.node[basin]['S_k'].copy()
        G.node[basin]['R_k'] = cost_function(G.node[basin]['A_k'],G.node[basin]['S_k'],
                                             G.node[basin]['retention_basin_properties'],
                                             G.node[basin]['watershed_properties'],
                                             G.node[basin]['subsurface_properties'])

        G.node[basin]['f_k'] = G.node[basin]['R_k']
        G.node[basin]['upstream_optimal_k'] = None
        return G
    else:
        children = G.neighbors(basin) #Directed Graph, with children as out-edges
        for child in children: #loop over children and compute costs
            #should compute T_k, R_k, S_k, A_k, f_k, possibly optimal upstream storage options (upstream_optimal_k) for each ntrial options
            G = optimize_storage_iteration_user_cost(G,child,cost_function,ntrial=ntrial,enforce_total_runoff_constraint=enforce_total_runoff_constraint,network_cost_function=network_cost_function)
            for key in ['S_k','T_k','R_k','S_k','A_k','f_k','upstream_optimal_k']:
                assert G.node[child].has_key(key), "could not find key = %s " % key
        #end first child step
        nchildren = len(children)
        #just try to get something working
        assert nchildren <= 2, "only testing for 1 or 2 children right now"
        #define all possible combinations of upstream storages ...
        table_dim = [ntrial]*nchildren
        #T_nm1ik[l,m] = T_{n-1,0,l} + T_{n-1,1,m} 
        G.node[basin]['T_nm1ik'] = np.zeros(tuple(table_dim),'d') 
        G.node[basin]['f_nm1ik'] = np.zeros(tuple(table_dim),'d')
        for k in range(ntrial):
            G.node[basin]['T_nm1ik'][k,...] = G.node[children[0]]['T_k'][k]
            G.node[basin]['f_nm1ik'][k,...] = G.node[children[0]]['f_k'][k]
        if nchildren > 1:
            for k in range(ntrial):
                G.node[basin]['T_nm1ik'][...,k] += G.node[children[1]]['T_k'][k]
                G.node[basin]['f_nm1ik'][...,k] += G.node[children[1]]['f_k'][k]
        #

        #for each candidate total storage at this node
        # 1. compute local storage needed for each combination of upstream runoff
        # 2. compute cost for each combination of local storage and upstream runoff
        # 3. pick minimum one and save value and cost in arrays T_k, R_k, f_k
        # 4. record the optimal upstream contribution for that k in ['upstream_optimal_k'][child][k]
        #
        #optimal upstream contributions for each k
        G.node[basin]['upstream_optimal_k'] = {}
        for m in range(nchildren):
            G.node[basin]['upstream_optimal_k'][children[m]] = {}
        #
        for k in range(ntrial):                    
            #local storage needed for this candidate runoff  
            S_ik = np.minimum(TRO_nik[k] - G.node[basin]['T_nm1ik'],G.node[basin]['retention_basin_properties']['S_pmax'])
            S_ik = np.where(S_ik < 0.0, 0.0, S_ik)
            T_ik = S_ik + G.node[basin]['T_nm1ik']
            #area associated with the storage
            if G.node[basin]['retention_basin_properties']['S_pmax'] > 0.:
                A_ik = S_ik/G.node[basin]['retention_basin_properties']['Y_max']
            else:
                A_ik = np.zeros(S_ik.shape,'d')
            #cost of the local storage
            R_ik  = cost_function(A_ik,S_ik,
                                  G.node[basin]['retention_basin_properties'],
                                  G.node[basin]['watershed_properties'],
                                  G.node[basin]['subsurface_properties'])
                                  
            f_ik = R_ik + G.node[basin]['f_nm1ik']

            if is_root and enforce_total_runoff_constraint: #enforce constraint on total outflow
                f_ik = np.where(T_ik < TRO_nik[k], sys.float_info.max, f_ik)
            elif is_root and network_cost_function != None:
                f_ik += network_cost_function(A_ik,S_ik,T_ik,
                                              G.node[basin]['retention_basin_properties'],
                                              G.node[basin]['watershed_properties'],
                                              G.node[basin]['subsurface_properties'])
            min_index_flat = np.argmin(f_ik);
            min_index = np.unravel_index(min_index_flat,f_ik.shape)
            G.node[basin]['S_k'][k] = S_ik[min_index]
            G.node[basin]['A_k'][k] = A_ik[min_index]
            G.node[basin]['T_k'][k] = S_ik[min_index] + G.node[basin]['T_nm1ik'][min_index]
            G.node[basin]['R_k'][k] = R_ik[min_index]
            G.node[basin]['f_k'][k] = f_ik[min_index]
            for m in range(len(min_index)):
                G.node[basin]['upstream_optimal_k'][children[m]][k] = min_index[m]
        #end loop through different trial options

        return G

def traceback_optimal_network(G,basin,optimal_storage_index):
    """
    Set optimal values for local storage, cost, upstream storage assuming these have been computed using optimize_storage_iteration

    optimal_storage_index is supposed to be the optimal candidate storage value

    Saves values in

    S_opt : local storage volume
    A_opt : local storage area
    T_opt : total storage locally and upstream
    R_opt : cost of local storage
    f_opt : total cost
    k_opt : which candidate was optmial
    """

    #should be the same for all k
    G.node[basin]['S_opt']=G.node[basin]['S_k'][optimal_storage_index]
    G.node[basin]['A_opt']=G.node[basin]['A_k'][optimal_storage_index]
    G.node[basin]['T_opt']=G.node[basin]['T_k'][optimal_storage_index]
    G.node[basin]['R_opt']=G.node[basin]['R_k'][optimal_storage_index]
    G.node[basin]['f_opt']=G.node[basin]['f_k'][optimal_storage_index]
    G.node[basin]['k_opt']=optimal_storage_index
    if len(G.out_edges(basin)) > 0:
        children = G.neighbors(basin) #Directed Graph, with children as out-edges
        for child in children: #loop over children
            #pass the optimal choice of 
            G = traceback_optimal_network(G,child,G.node[basin]['upstream_optimal_k'][child][optimal_storage_index])
    #
    return G

def record_optimal_network_results(G,basin,display=False):
    """
    Output some of the results from the network optimization
    """
    s = """
T,S,A,R,f,k = ({:10.3f}, {:10.3f}, {:10.3f}, {:10.2f}, {:10.2f}, {:5d})
    """
    out = s.format(G.node[basin]['T_opt'],
                   G.node[basin]['S_opt'],
                   G.node[basin]['A_opt'],
                   G.node[basin]['R_opt'],
                   G.node[basin]['f_opt'],
                   G.node[basin]['k_opt'],
                   )
    if display is True:
     print "{s:*>10} basin {basin} {s:*<10}".format(s='',basin=basin)
     print out
  

    if len(G.out_edges(basin)) > 0: #loop over children
        children = G.neighbors(basin) #Directed Graph, with children as out-edges
        for child in children: #loop over children
            G = record_optimal_network_results(G,child,display=display)

    
    return G

def run_optimization_travis_mays(ntrial=3,cost_function=travis_and_mays_cost_function,display_results=False,use_original_implementation=False):
    """
    Go through the process of building and running the Travis and Mays example prolblem 
    """
    root = (4,1)
    G = build_example_network(use_travis_and_mays_props=True)
    G = compute_basin_properties(G)
    G = compute_total_runoff(G,root)
    #original, hardwired cost function
    if use_original_implementation:
        G = optimize_storage_iteration(G,root,ntrial,is_root=True)
    else:
        G = optimize_storage_iteration_user_cost(G,root,cost_function,ntrial,is_root=True)

    G = traceback_optimal_network(G,root,0)
    G = record_optimal_network_results(G,root,display=display_results)

    return G
    
def run_optimization_sensitivity(val_list,property_class,prop='L',cost_function=travis_and_mays_cost_function,ntrial=3,use_original_implementation=False):
    """
    Try to wrap Luke's sensitivity analysis into a separate function
    
    val_list -- values to test. val_list[j] --> node j value
    prop     -- what value is being tested
    ntrial   -- discretization level for dynamic programming approach
    """
    root = (4,1)
    data_list=[]
    G = build_example_network(use_travis_and_mays_props=True)
    G = compute_basin_properties(G)
    node_list = [(1,1),(2,1),(2,2),(2,3),(3,1),(3,2),(4,1)] #use (isodrainage line,index) convention
    for j,nodes in enumerate(node_list):
        assert G.node[nodes].has_key(property_class)
        G.node[nodes][property_class][prop]=val_list[j]
    G = compute_total_runoff(G,root)
    if use_original_implementation:
        #original, hardwired cost function
        G = optimize_storage_iteration(G,root,ntrial,is_root=True)
    else:
        #should give the same result as the hard-wired function
        G = optimize_storage_iteration_user_cost(G,root,travis_and_mays_cost_function,ntrial,is_root=True,enforce_total_runoff_constraint=True)
    G = traceback_optimal_network(G,root,0)
    G = record_optimal_network_results(G,root)
    for nodes in node_list:
        data_list.append(G.node[nodes][property_class][prop])
    return G,data_list

def run_optimization_travis_mays_recharge_formulation(ntrial=3,cost_function=travis_and_mays_cost_function,display_results=False,use_original_implementation=False):
    """
    Version of the Travis and Mays example that relaxes the total runoff constraint and uses the cost function

    total_cost = \alpha_0*|(T_1-Q_0)| + \sum_{i}R(S_i), where i=1,number of nodes
    
    """
    root = (4,1)
    G = build_example_network(use_travis_and_mays_props=True)
    G = compute_basin_properties(G)
    G = compute_total_runoff(G,root)
    #set the necessary parameters for the recharge constraint
    #set the recharge goal to be one third of the maximum run-off
    TRO = 0
    for basin in G.nodes():
        TRO += G.node[basin]['watershed_properties']['SRO']
    Q_0 = 0.66*TRO
    print "+++++ Testing Recharge Formulation with target Recharge Q_0 = %s, %s percent of total runoff= %s +++++" % (Q_0,Q_0/TRO*100,TRO)
    G.node[root]['watershed_properties']['total_recharge_target']=Q_0
    G.node[root]['watershed_properties']['recharge_weight'] = 40.0
    G = optimize_storage_iteration_user_cost(G,root,cost_function,ntrial,is_root=True,enforce_total_runoff_constraint=False,
                                             network_cost_function=network_recharge_cost_function)
    min_root_k = np.argmin(G.node[root]['f_k'])
    G = traceback_optimal_network(G,root,min_root_k)
    G = record_optimal_network_results(G,root,display=display_results)

    return G

def run_optimization_sensitivity_travis_mays_recharge(val_list,property_class,prop='L',cost_function=travis_and_mays_cost_function,ntrial=3,use_original_implementation=False):
    """
    Try to wrap Luke's sensitivity analysis into a separate function

    Uses recharge formulation for objective function

    val_list -- values to test. val_list[j] --> node j value
    prop     -- what value is being tested
    ntrial   -- discretization level for dynamic programming approach
    """
    root = (4,1)
    data_list=[]
    G = build_example_network(use_travis_and_mays_props=True)
    G = compute_basin_properties(G)
    #if you want a fixed target based on the original network
    use_original_runoff_target = False
    if use_original_runoff_target:
        G = compute_total_runoff(G,root)
        #set the recharge goal to be two-thirds of the maximum run-off
        TRO = 0
        for basin in G.nodes():
            TRO += G.node[basin]['watershed_properties']['SRO']
        Q_0 = 0.66*TRO
    #hard_wired to nodes in the Travis and Mays network
    node_list = [(1,1),(2,1),(2,2),(2,3),(3,1),(3,2),(4,1)] #use (isodrainage line,index) convention
    for j,nodes in enumerate(node_list):
        assert G.node[nodes].has_key(property_class)
        G.node[nodes][property_class][prop]=val_list[j]
    #recompute the basin properties now that the target property has been perturbed
    G = compute_basin_properties(G)
    G = compute_total_runoff(G,root)    
    if not use_original_runoff_target:
        #set the recharge goal to be two-thirds of the maximum run-off
        TRO = 0
        for basin in G.nodes():
            TRO += G.node[basin]['watershed_properties']['SRO']
        Q_0 = 0.66*TRO
    #
    print "+++++ Testing Recharge Formulation with target Recharge Q_0 = %s, %s percent of total runoff= %s +++++" % (Q_0,Q_0/TRO*100,TRO)
    G.node[root]['watershed_properties']['total_recharge_target']=Q_0
    G.node[root]['watershed_properties']['recharge_weight'] = 40.0
    G = optimize_storage_iteration_user_cost(G,root,cost_function,ntrial,is_root=True,enforce_total_runoff_constraint=False,
                                             network_cost_function=network_recharge_cost_function)

    min_root_k = np.argmin(G.node[root]['f_k'])
    G = traceback_optimal_network(G,root,min_root_k)
    G = record_optimal_network_results(G,root)
    for nodes in node_list:
        data_list.append(G.node[nodes][property_class][prop])
    return G,data_list

def test_sensitivity(property_class,prop):
    """
    Trying to extracting the sensitivity run as a separate routine
    """
    data_list=[]
    node_list = [(1,1),(2,1),(2,2),(2,3),(3,1),(3,2),(4,1)] #use (isodrainage line,index) convention
    for l1 in [0.5,0.75,1.5]:
      for l2 in [0.5,0.75,1.5]:
        for l3 in [0.5,0.75,1.5]:
          for l4 in [0.5,0.75,1.5]:
            for l5 in [0.5,0.75,1.5]:
              for l6 in [0.5,0.75,1.5]:
                for l7 in [0.5,0.75,1.5]:
                 price_list=[l1,l2,l3,l4,l5,l6,l7]
                 G,data_list= run_optimization_sensitivity(price_list,property_class,prop=prop,ntrial=10)
                 data_list.append(G.node[(4,1)]['f_opt'])
                 print data_list

def test_sensitivity_recharge_example():
    """
    Trying to extracting the sensitivity run as a separate routine for Rebecca and Rachel
    """
    property_class = 'watershed_properties' #what type of property to change
    prop = 'I_m' #actual property
    val_range = np.arange(1,11)*7.6*0.005 #range of values to try
    optimal_values = []
    #nodes in basin
    node_list = [(1,1),(2,1),(2,2),(2,3),(3,1),(3,2),(4,1)] #use (isodrainage line,index) convention
    root = (4,1) #base of network
    for val in val_range:
        #set the same value for every node in the basin
        node_values=[val for node in node_list]
        G,data_list= run_optimization_sensitivity_travis_mays_recharge(node_values,property_class,prop=prop,ntrial=10)
        optimal_values.append(G.node[root]['f_opt'])
    return val_range,optimal_values
    

def test_optimization_travis_mays(ntrial=10,plot_network=False):
    #print out network
    G = build_example_network(use_travis_and_mays_props=True)
    G = compute_basin_properties(G,display=True)
    if plot_network:
        plot_network(G,'travis_mays_example_network')

    print "Running optimization -----"

    G= run_optimization_travis_mays(ntrial=ntrial,display_results=True)

def test_optimization_travis_mays_new_formulation(ntrial=10,plot_network=False):
    #print out network
    G = build_example_network(use_travis_and_mays_props=True)
    G = compute_basin_properties(G,display=True)
    if plot_network:
        plot_network(G,'travis_mays_example_network')

    print "Running optimization -----"
    G= run_optimization_travis_mays_recharge_formulation(cost_function=travis_and_mays_cost_function,ntrial=ntrial,display_results=True)


if __name__ == "__main__":

   
   try_sensitivity = True
   if try_sensitivity:
       f = open('sentivity.dat','w')
       val_range,optimal_values = test_sensitivity_recharge_example()
       for val,opt in zip(val_range,optimal_values):
           f.write('{0} {1}\n'.format(val,opt))
       f.close()
   else:
       test_infil()
       test_runoff()
       test_optimization_travis_mays()
       test_optimization_travis_mays_new_formulation()
   
