						/*******************************/ 
						/*         intercept.c         */ 
						/*******************************/ 


#include "all.h"

extern void intercept(int j, int k)
{

	if (rinter[j][k] * dt >= ret[j][k])
	{
		rinter[j][k] = rinter[j][k] - ret[j][k] / dt;
		ret[j][k] = 0;
	}
	else
	{
		ret[j][k] = ret[j][k] - rinter[j][k] * dt;
		rinter[j][k] = 0;
	}

}
