import sys
import os
import math

""" 
 This script reads specified directory and rewrites the data to .m format.
   To Run:
   [user@machine directory]$ python ConvertWater.py 

"""

# ============================================================================ #
# Run Time options                                                             #
# ============================================================================ #
matlabfile       = "Data.m"        # Name of the matlab file to write the coefficents to. 
Directory        = "/Users/lea/John/CASC2D-SED-Code/Example/Outmaps/"
DirectoryOut     = Directory

FileBase         = "rain"

sys.path.append(Directory)

# ============================================================================ #
# Local Variables                                                              #

# ============================================================================ #
totalFrames   = 1400

for fileset in range(totalFrames): 

    fileindex = fileset + 1; 
    # =========================================================================
    # Open a matlab file to write the data to.   
    # =========================================================================
    matlabfile = "MatlabData" + str('%(#)08d.m' % {"#":fileindex})
    matlabfile = DirectoryOut + matlabfile
    MATLAB = open(matlabfile,"w")
    #STRING = "% Writing a .m file for the stress tensor at time t = " + str(fileindex) + " \n"; MATLAB.write(STRING); 
 
    # =========================================================================
    # open the data file that contains the original data #
    # ========================================================================= 
    datFile = Directory + FileBase + str('.') + str(fileindex) #str('%(#)08d.vtk' % {"#":fileset})
    myfile  = open(datFile,"r")
    HeaderFlag = 0; 
    for line in myfile.readlines():
        if (HeaderFlag == 1):
            numbers = map(str, line.split())
            for value in range(len(numbers)):
                if(numbers[value] != -9999): 
                    STRING = ' ' + str(numbers[value]) + ' '; 
                if(str(numbers[value]) == str(-9999)):
                    STRING = str(' NaN '); 
                MATLAB.write(STRING);
            STRING = "\n"; MATLAB.write(STRING);
        if (HeaderFlag == 0):
            numbers = map(str, line.split())
            if(str(numbers[0]) == str('NODATA_value')):
                HeaderFlag = 1; 
 
    #STRING = "]; \n\n"; MATLAB.write(STRING); 

    MATLAB.write("\n\n")
    MATLAB.close()



