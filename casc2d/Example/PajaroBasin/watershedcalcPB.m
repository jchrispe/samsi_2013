% Code for preprocessing DEM data to choose optimal location of retention
% basins and connectivity relationships betweent the chosen basins
%%%

% Reading DEM data for PajaroBasin and plotting it %%%%
clear all
clc
[dem,X,Y] = rasterread('../../samsi_2013/casc2d/Example/PajaroBasin/Input/elevation.asc');
figure(1)
surf(X,Y,dem);
axis image;
shading interp;
title('DEM Data');
camlight
export_fig dem.png -q300;

%defining variables for subsequent calculations out of imported data %%
cs  = abs(Y(2)-Y(1));
siz = size(dem);

%Filling erroneous topographic depressions in DEM data %%%
dem = fillsinks(dem);

% Computing topographic derivates like Hillshape, slope, curvatures etc.
H = hillshade(X,Y,dem,315,60);
G = gradient8(dem,cs);
[prof,plan] = curvature(dem,cs);

%plotting the derivates
figure(2)
imagesc(X(1,:),Y(:,2),H); axis image; axis xy; colormap(gray);
title('Hillshading')
drawnow
export_fig ../../samsi_2013/casc2d/Example/PajaroBasin/Preproc/hillshade.png -q300;

figure(3)
colormap('default')
imageschs(X,Y,dem,G); colorbar;
title('Slope')
drawnow
export_fig ../../samsi_2013/casc2d/Example/PajaroBasin/Preproc/slope.png -q300;


% figure(4)
% imageschs(X,Y,dem,prof);
% colorbar
% title('Profile curvature')
% drawnow
% 
% 
% figure(5)
% colormap('default')
% imageschs(X,Y,dem,plan);
% colorbar
% title('Planform curvature')
% drawnow

%Computing flow-direction matrix and flow accumulation matrix with single flow
Ms = flowdir(X,Y,dem,'type','single');
% figure(6)
% spy(Ms)
% title('Sparsity pattern of the single flow direction matrix')

As = flowacc(Ms,siz);
figure(7)
imagesc(X(1,:),Y(:,2),As); axis image; axis xy; colorbar;
title('Flow accumulation based on single flow direction matrix')
export_fig ../../samsi_2013/casc2d/Example/PajaroBasin/Preproc/flowaccm-single.png -q300;

%Computing flow-direction matrix with multiple flow (not useful for some
%subsequent processing
Mm = flowdir(X,Y,dem,'type','multi');
% figure(8)
% spy(Mm)
% title('Sparsity pattern of the multiple flow direction matrix')


Am = flowacc(Mm,siz);
figure(9)
imagesc(X(1,:),Y(:,2),log(Am)); axis image; axis xy; colorbar
title('Logarithm of flow accumulation based on multiple flow direction matrix')
export_fig ../../samsi_2013/casc2d/Example/PajaroBasin/Preproc/flowaccm-multi.png -q300;


% calculate flow accumulation and direction
% let's simply assume that channels start where
% A is larger than 120;
Ws = As>100;
% and calculate the strahler stream order ...
[S,nodes] = streamorder(Ms,Ws);
% ... and visualize it
figure(10)
subplot(1,2,1);
pcolor(X,Y,+Ws); axis image; shading flat; colorbar
title('Stream Network')
subplot(1,2,2);
pcolor(X,Y,S); axis image; shading flat; colorbar;
hold on
plot(X(nodes),Y(nodes),'ks','MarkerFaceColor','g')
title('Strahler Stream Order')
export_fig ../../samsi_2013/casc2d/Example/PajaroBasin/Preproc/stream-network.png -q300;

% Computing drainage basin which is the same as sub-watershed delineation
% (it only takes single flow direction matrix as input)
order = 1; % order of stream
DB = drainagebasins(Ms,S,order);
nrDB = numel(unique(DB+1)); % nr of drainage basins

STATS = regionprops(DB+1,'PixelIdxList','Area','Centroid');
figure(11)
imageschs(X,Y,dem,shufflelabel(DB)); colorbar;

hold on
for run = 1:nrDB;
    text(X(1,round(STATS(run).Centroid(1))),Y(round(STATS(run).Centroid(2)),2),...
         num2str(round(STATS(run).Area * cs))); 
end
% superimpose basin locations on sub-watershed plot
plot(X(nodes),Y(nodes),'ks','MarkerFaceColor','k')
title('sub-watershed delineation and retention basin locations');
export_fig ../../samsi_2013/casc2d/Example/PajaroBasin/Preproc/sub-watershed.png -q300;



%Compute drainage area for basin locations in map
B = false(size(dem));
figure(12)
xi = nodes; 
for run=1:numel(xi)
B(xi(run)) = true;
D = dependencemap(Ms,B);
D = +D;
D(B) = D(B)+2;

imageschs(X,Y,dem,D); axis image; axis xy
hold on
% superimpose basin locations on sub-watershed plot
plot(X(nodes),Y(nodes),'ks','MarkerFaceColor','k')
% contour(X,Y,dem,30,'k')
caxis([0 2])
end


%%% Computing cells downstream that are influenced by water at point of interest     
% L = false(size(dem));
% ix = nodes(7); % location of interest
% L(ix) = true;
% [I,Msmod] = influencemap(Ms,L);
% I = +I;
% I(L) = I(L)+2;
% figure(13)
% subplot(1,2,1);
% gplot(Msmod,[X(:) Y(:)]); axis image; shading flat;
% colorbar
% title('Modified Stream Network')
% subplot(1,2,2);
% imagesc(X(1,:),Y(:,2),I); axis image; axis xy 
% colorbar
% hold on
% contour(X,Y,dem,30,'k')
% caxis([0 2])
% hold off