#! /usr/bin/env python

import numpy as np

def read_casc2d_header(filename):
    """
    read a casc2d file header and return 

ncols		number of columns in raster grid
nrows		number of rows in raster grid
xllcorner	bounding box reference
yllcorner	
cellsize	dx=dy
NODATA_value	value means not modelled
 
value           numpy array of shape (nrows,ncols)
    """
    fd = open(filename,'r')

    line = fd.readline()
    ncols = int(line.split()[1])
    
    line = fd.readline()
    nrows = int(line.split()[1])

    line = fd.readline()
    xll   = float(line.split()[1])

    line = fd.readline()
    yll   = float(line.split()[1])

    line = fd.readline()
    dx    = float(line.split()[1])

    line = fd.readline()
    NODATA_value = float(line.split()[1])

    fd.close()

    return ncols,nrows,xll,yll,dx,NODATA_value

def read_casc2d_outmap(filename):
    """
    read a casc2d file return 

    value           numpy array of shape (nrows,ncols)

    """
    
    value = np.loadtxt(filename,skiprows = 6)
    return value

    

    
def test_plot(filename):
    import matplotlib.pyplot as plt

    #nc defines number of cells in x direction
    #nr defines number of cells in y direction
    nc,nr,xll,yll,dx,nodata = read_casc2d_header(filename)
    data = read_casc2d_outmap(filename)
    masked_data = np.ma.masked_where(data == nodata,data)
    #note reversal of nc, nr
    X = np.arange(xll,xll+dx*nc,dx)
    Y = np.arange(yll,yll+dx*nr,dx)
    X,Y = np.meshgrid(X,Y)
    
    fig = plt.figure()
    ax = fig.gca()


    image = ax.imshow(masked_data,interpolation='nearest',extent=(xll,xll+dx*nc,yll,yll+dx*nr))
    fig.colorbar(image)
    ax.set_title(filename)
    
    plt.show()
    plt.savefig(filename+'.png')

def test_animation(filebase,verbose=1):
    #setup animation
    import matplotlib
    matplotlib.use("Agg")
    import matplotlib.pyplot as plt
    import matplotlib.animation as animation
    
    #mwf debug
    #import pdb
    #pdb.set_trace()
    #Writer = animation.writers['ffmpeg']
    #writer = Writer(fps=15,metadata=dict(artist='Foo'),bitrate=1800)

    #generate all of the figures
    fig = plt.figure()

    import glob
    files = glob.glob(filebase+'.*[0-9]')
    files_to_remove = []
    for name in files:
        if name.split('.')[-1] == 'mp4':
            files_to_remove.append(name)
    for name in files_to_remove:
        files.remove(name)
    def suff_comp(x,y):
        sx = int(x.split('.')[-1])    
        sy = int(y.split('.')[-1])
        if sx < sy:
            return -1
        if sx > sy:
            return 1
        return 0
    #
    files.sort(cmp=suff_comp)
    if verbose > 0:
        print "trying to read {} files with base {}".format(len(files),filebase)   
    #store images here
    ax = fig.gca()
    
    ims = []
    for file_step in files:
        #nc defines number of cells in x direction
        #nr defines number of cells in y direction
        if verbose > 0:
            print "reading {}".format(file_step)
        nc,nr,xll,yll,dx,nodata = read_casc2d_header(file_step)
        data = read_casc2d_outmap(file_step)
        masked_data = np.ma.masked_where(data == nodata,data)

 
        ims.append([plt.imshow(masked_data,interpolation='nearest',
                             extent=(xll,xll+dx*nc,yll,yll+dx*nr))])
    #end plot loop
    #mwf debug
    #import pdb
    #pdb.set_trace()
    im_ani = animation.ArtistAnimation(fig,ims,interval=200,repeat_delay=3000,blit=True)
    im_ani.save(filebase+'.mp4')#writer=writer)
                

            
if __name__ == "__main__":

    import os,optparse

    usage = "usage: %prog map_file_to_print"
    parser = optparse.OptionParser(usage)
    parser.add_option("-a","--animation",
                      help="Generate an animation",
                      action="store_true",
                      default=False)
    
    (opts,args) = parser.parse_args()
    if len(args) < 1:
        print "Too few arguments: {}. Must specify name of file to plot".format(args)
        sys.exit(1)
    if not opts.animation:
        for f in args:
            test_plot(f)
    else:
        test_animation(args[0])
        
