#! /usr/bin/env python

import numpy as np

import process_map as pm

def collect_time_series(filebase,t0,dt,i,j,verbose=0):
    """
    extract values stored in filebase.* in column i,j
    time is t0 + dt*k
    """

    import glob
    files = glob.glob(filebase+'.*[0-9]')
    files_to_remove = []
    for name in files:
        if name.split('.')[-1] == 'mp4':
            files_to_remove.append(name)
    for name in files_to_remove:
        files.remove(name)

    def suff_comp(x,y):
        sx = int(x.split('.')[-1])    
        sy = int(y.split('.')[-1])
        if sx < sy:
            return -1
        if sx > sy:
            return 1
        return 0
    #
    files.sort(cmp=suff_comp)
    if verbose > 0:
        print "trying to read {} files with base {}".format(len(files),filebase)   
    #
    tvals = []; vals = []
    for k,file_step in enumerate(files):
        if verbose > 0:
            print "reading {}".format(file_step)
        nc,nr,xll,yll,dx,nodata = pm.read_casc2d_header(file_step)
        data = pm.read_casc2d_outmap(file_step)
        tvals.append(t0+k*dt);
        vals.append(data[i-1,j-1])
    #
    ts = np.array(tvals); vs = np.array(vals)
    return ts,vs

def save_timeseries_to_file(filebase,t0,dt,i,j):
    """
    save value in cell i,j (base 1) from output files filebase.*
    """
    ts,vals = collect_time_series(filebase,t0,dt,i,j)
    assert len(ts) == len(vals)

    fname = filebase+"_cell_{i}_{j}.txt".format(i=i,j=j)
    f = open(fname,'w')
    for t,v in zip(ts,vals):
        f.write("{t:12.5e}  {v:12.5e} \n".format(t=t,v=v))
    #
    f.close()

def plot_timeseries(filebase,t0,dt,i,j):
    """
    plot a time series of values
    """
    ts,vals = collect_time_series(filebase,t0,dt,i,j)
    assert len(ts) == len(vals)

    import matplotlib.pyplot as plt
    fig = plt.figure()
    ax = fig.gca()

    plt.plot(ts,vals)
    ax.set_title(filebase+" for cell ({0},{1})".format(i,j))

    fname = filebase+"_cell_{i}_{j}.png".format(i=i,j=j)

    plt.show()
    plt.savefig(fname)

def read_hydrographs(filename):
    """
    read the hydrographs stored in the casc2d .q file and return them in a dictionary of arrays

    hydrograh['t'] time values
    hydrograph[RowiColj] = time series of flow in cell i,j 
    """

    f = open(filename,'r')
    lines = f.readlines()
    nt = len(lines)-1

    header = lines[0]
    tmp = header.split()
    nbasins = len(tmp)-1
    keys = tmp[1:]
    
    output = {}
    output['t'] = np.zeros(nt,'d')
    for key in keys:
        output[key] = np.zeros(nt,'d')

    for i,line in enumerate(lines[1:]):
        vals = line.split()
        output['t'][i] = float(vals[0])
        for k,Q in zip(keys,vals[1:]):
            output[k][i] = float(Q)
    #
    return output

def accumulate_hydrographs(hydro_output):
    """
    assumes dt is constant
    """
    nbasins = len(hydro_output.keys())-1 ; nt = len(hydro_output['t'])
    Qsum = {}; 
    Qsum['t'] = hydro_output['t'].copy()
    dt = Qsum['t'][1]-Qsum['t'][0]
    for k in hydro_output.keys():
        if k != 't':
            Qsum[k] = np.cumsum(hydro_output[k])
            Qsum[k] *= dt

    return Qsum
    
            
if __name__ == "__main__":

    import os,optparse

    usage = "usage: %prog  [options] "
    parser = optparse.OptionParser(usage)
    parser.add_option("-i","--cell_i",
                      help="row index of cell (base 1)",
                      action="store",
                      type="int",
                      default=None)
    parser.add_option("-j","--cell_j",
                      help="col index of cell (base 1)",
                      action="store",
                      type="int",
                      default=None)
    parser.add_option("-f","--filebase",
                      help="base of file series from which to extract values",
                      action="store",
                      type="string",
                      default=None)
    parser.add_option("--dt",
                      help="timestep for simulation",
                      action="store",
                      type="float",
                      default=1.0)
    parser.add_option("--t0",
                      help="initial time for simulation",
                      action="store",
                      type="float",
                      default=0.0)
    parser.add_option("-P","--plot",
                      help="plot time series?",
                      action="store_true",
                      default=False)
    
    
    (opts,args) = parser.parse_args()

    assert opts.filebase != None
    assert opts.cell_j   != None
    assert opts.cell_i   != None

    save_timeseries_to_file(opts.filebase,opts.t0,opts.dt,opts.cell_i,opts.cell_j)

    if opts.plot:
        plot_timeseries(opts.filebase,opts.t0,opts.dt,opts.cell_i,opts.cell_j)
        
