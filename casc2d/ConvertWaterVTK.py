import sys
import os
import math

""" 
 This script reads specified directory and rewrites the data to .vtk format.
   To Run:
   [user@machine directory]$ python ConvertWaterVTK.py 

"""

# ============================================================================ #
# Run Time options                                                             #
# ============================================================================ #
Directory        = "/Users/lea/John/CASC2D-SED-Code/PajaroBasin/Outmaps/"
DirectoryOut     = Directory

FileBase         = "depth"

sys.path.append(Directory)

# ============================================================================ #
# Local Variables                                                              #
# ============================================================================ #
totalFrames   = 1400

for fileset in range(totalFrames): 
    fileindex = fileset + 1; 

    # =========================================================================
    # open the data file that contains the original data 
    # ========================================================================= 
    datFile = Directory + FileBase + str('.') + str(fileindex) 
    #print "Reading ", datFile
    datafile  = open(datFile,"r")
    HeaderFlag = 0; 
    lineseen = 0;
    # =========================================================================
    # Open a matlab file to write the data to.   
    # =========================================================================
    VTKfile = FileBase + str('%(#)08d.vtk' % {"#":fileindex})
    VTKfile = DirectoryOut + VTKfile
    #print "Write ", VTKfile
    FILE = open(VTKfile,"w")

    for line in datafile.readlines():
        # =====================================================================
        # Read out the Header
        # =====================================================================
        if (HeaderFlag == 0):
            numbers = map(str, line.split())
            if(str(numbers[0]) == str('NODATA_value')):
                noDataValue = str(numbers[0]);
                lineseen = lineseen + 1
            if(str(numbers[0]) == str('ncols')):
                xpts = int(numbers[1]); 
                lineseen = lineseen + 1
            if(str(numbers[0]) == str('nrows')):
                ypts = int(numbers[1]); 
                lineseen = lineseen + 1
            if(str(numbers[0]) == str('xllcorner')):
                xllcorner = float(numbers[1]); 
                lineseen = lineseen + 1
            if(str(numbers[0]) == str('yllcorner')):
                yllcorner = float(numbers[1]); 
                lineseen = lineseen + 1
            if(str(numbers[0]) == str('cellsize')):
                cellsize = float(numbers[1]); 
                lineseen = lineseen + 1

            if(lineseen == 6):
                HeaderFlag = 1; 
                string = "# vtk DataFile Version 2.0\n"; FILE.write(string);
                string = "Paraview Legacy File " + Directory + FileBase + str('_%(#)08d.vtk\n' % {"#":fileindex}); FILE.write(string);
                string = "ASCII \nDATASET RECTILINEAR_GRID\n"; FILE.write(string);
                string = "DIMENSIONS " + str(xpts) + " " + str(ypts) + " 1\n";  FILE.write(string);
                string = "X_COORDINATES " + str(xpts) + " double\n";  FILE.write(string);
                string = ""; 
                for i in range(xpts):
                    # print the x coordinates 
                    string = string + str(xllcorner + (0.5+i)*cellsize) + " " ;
                string = string + "\n"; FILE.write(string);
                string = "Y_COORDINATES " + str(ypts) + " double\n";  FILE.write(string);
                string = ""; 
                for i in range(ypts):
                    # print the y coordinates                     
                    string = string + str(yllcorner + ypts*cellsize - (0.5+i)*cellsize) + " " ;
                string = string + "\n"; FILE.write(string);
                string = "Z_COORDINATES 1 double\n0\n";  FILE.write(string);
                string = "POINT_DATA " + str(xpts*ypts) + "\n";  FILE.write(string);
                string = "SCALARS " + FileBase + " double 1\nLOOKUP_TABLE default\n"; FILE.write(string);

        # =========================================================================
        # Loop over the data in the file   
        # =========================================================================
        if (HeaderFlag == 1):
            if(lineseen > 6):
                numbers = map(str, line.split())
                for value in range(len(numbers)):
                    if(numbers[value] != -9999): 
                        STRING = str(numbers[value]) + " \n"; 
                    if(str(numbers[value]) == str(0)):
                        STRING = str('-1.0 \n'); 
                    FILE.write(STRING);
            lineseen = lineseen + 1; 


    FILE.write("\n\n")
    FILE.close()

        

