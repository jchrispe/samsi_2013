#! /usr/bin/env python
import numpy as np
from preproc import flood_fill
from math import sqrt

def d8(DX,DY,dem,fill_pits=False,route_flag=0,use_mask=False):
    """
    Compute flow direction on DEM using simple d8 algorithm

    Returns flow matrix,  \mat{M} \in R^{nxn}, with
    
      M_{ij} = 1 if cell i flows to cell j, zero otherwise

      Note, M_{ii} = 0, and \sum_{i} M_{ij} = number of cells that flow to cell j 
      
    Assumes 'matrix' view of domain

    neighbor convention

    3  2  1          (i-1,j-1)  (i-1,j) (i-1,j+1)                                      
    4     0     ---> (i  ,j-1)          (i  ,j+1)  , matlab style convention (i,j) --> j*dem.shape[0] + i  --> 
    5  6  7          (i+1,j-1)  (i+1,j) (i+1,j+1)

    """
    if fill_pits:
        dem = flood_fill(dem,use_masked_input=use_mask)
    nr = dem.shape[0]
    nc = dem.shape[1]
    HYP = sqrt(DX*DX + DY*DY)
    
    ghost = np.zeros((nr+2,nc+2),'d')
    if use_mask:
        ghost = np.ma.asarray(ghost)
        
    ghost[1:-1,1:-1] = dem[:,:]
    ghost[0,1:-1]  = dem[0,:]
    ghost[-1,1:-1] = dem[-1,:]
    ghost[1:-1,0]  = dem[:,0]
    ghost[1:-1,-1] = dem[:,-1]
    
    ghost[0,0]=ghost[1,1]; ghost[-1,-1]=ghost[-2,-2];
    ghost[0,-1]=ghost[1,-2]; ghost[-1,0]=ghost[-2,1]

    
    neig_incr_row_major = np.array([1,
                                    -nc+1,-nc,-nc-1,
                                    -1,
                                    -1+nc,nc,nc+1])
    neig_incr_col_major = np.array([nr,
                                    nr-1,-1,-nr-1,
                                    -nr,
                                    -nr+1,1,nr+1])
                         
    neig_incr = neig_incr_col_major
    slopes = np.zeros((8,),'d')
    all_indeces = np.arange(nr*nc,dtype='i')
    max_indeces = np.zeros(nr*nc,'i')
    slope_vals  = np.zeros(nr*nc,'d')
    slope_count = np.zeros(nr*nc,'i')
    
    for i in range(1,nr+1):
        for j in range(1,nc+1):
            slopes.fill(0)
            if not use_mask or not ghost.mask[i,j]: #really brain dead
                #
                slopes[0] = (ghost[i,j]-ghost[i,j+1])/DX
                slopes[4] = (ghost[i,j]-ghost[i,j-1])/DX
                #
                slopes[1] = (ghost[i,j]-ghost[i-1,j+1])/HYP
                slopes[2] = (ghost[i,j]-ghost[i-1,j])/DY
                slopes[3] = (ghost[i,j]-ghost[i-1,j-1])/HYP
                #
                slopes[5] = (ghost[i,j]-ghost[i+1,j-1])/HYP
                slopes[6] = (ghost[i,j]-ghost[i+1,j])/DY
                slopes[7] = (ghost[i,j]-ghost[i+1,j+1])/HYP

                #glob_ind = (i-1)*nc + j-1 #local cell row major
                glob_ind = (j-1)*nr + i-1 #local cell col major
                loc_max  = slopes.argmax()
                if slopes[loc_max] > 0:
                    glob_max = min(max(glob_ind + neig_incr[loc_max],0),nc*nr-1)
                    max_indeces[glob_ind] = glob_max
                    slope_vals[glob_ind]  = slopes[loc_max]
                    slope_count[glob_ind] = 1


                
                    
        #
    #
        
    from scipy import sparse
    #M = sparse.csr_matrix((slope_vals,(all_indeces,max_indeces)),shape=(nr*nc,nr*nc))
    M = sparse.csr_matrix((slope_count,(all_indeces,max_indeces)),shape=(nr*nc,nr*nc))

    #now apply crossing flats algorithm
    if route_flag == 0: #barnes
        import flat_resolution as fres
        fmask,flabels,fheight,neigs = fres.resolve_flats(dem,M,use_mask=use_mask)
        #this is slow, need to change to adjust before building sparse matrix!
        fres.adjust_flow_matrix_d8(M,neigs,fmask,flabels)
    return M

def flowacc(M,dsh):
    """
    Given flow matrix, M, compute total number of cells contributing to each cell

      \mat{M} \in R^{nxn}
      M_{ij} = 1 if cell i flows to cell j, zero otherwise

      Note, M_{ii} = 0, and \sum_{i} M_{ij} = number of cells that flow to j 
      
    Solve for a_k, total number of cells contributing to cell k
    Define the cells that contribute to k as
 
       \mathcal{I}(k) = \{i : M_{ij} = 1 \}

    Then we can define a_i as
    
       a_i = 1 + \sum_{j \in \mathcal{I}(i)} a_j
    or
       a_i = 1 + \sum_{j=1}^{n} M_{ji}a_j

    In other words, solve the equation

       \left(\mat{I} - \mat{M}^{'}\right) \vec a = \vec 1
       
    Input
      M sparse flow matrix size with logical size nxn
      
      dsh size of dem with n = dsh[0]*dsh[1]

    Output

      a, array holding total number of cells contributing to each cell
    """
    from scipy import sparse as sp
    from scipy.sparse import linalg as splg
    nc = M.shape[1]
    I = sp.eye(M.shape[0],M.shape[1])
    B = I-M.transpose()
    b = np.ones(nc,'d')
    a = splg.spsolve(B,b)
    a = a.reshape(dsh,order='F')
    return a

def test0():
    """

    """
    dem  = np.array([[13,14,14,13,12],
                     [12,13,15,14,11],
                     [12,11,13,10,10],
                     [10,10,12,11,9 ],
                     [8,9,11,8,10]],'d')

    
    DX =  DY = 1.0
    #
    demf = flood_fill(dem)
    #toy d8 flow algorithm
    M = d8(DX,DY,demf)
    #accumulated flow area
    A = flowacc(M,demf.shape)
    #compute basins
    from drainage import drainagebasins
    DB,outlets = drainagebasins(M,demf.shape)
    return dem,demf,M,A,DB,outlets


def test1(filename='exampleDEM.txt',route_flag=0,use_mask=False):
    """

    """
    from raster_input import read_raster
    X,Y,dem = read_raster(filename,mask=use_mask)
    DY = DX = X[0,1]-X[0,0]
    if use_mask and dem.mask.max() == False: #no masked values so turn off to save complexity
        use_mask = False
    import matplotlib.pyplot as plt

    filebase = filename.split('.')[0]
    
    fig = plt.figure()

    fig.clf()
    ax = fig.gca()
    ax.ticklabel_format(style='sci', axis='both', scilimits=(0,0))
    image = ax.imshow(dem,interpolation='nearest',extent=(X[0,0],X[0,-1],Y[0,0],Y[-1,0]))
    fig.colorbar(image)
    ax.set_title('Elevation')
    plt.show()
    plt.savefig(filebase+'_dem.png')
    
    from preproc import flood_fill
    demf = flood_fill(dem,use_masked_input=use_mask)

    fig.clf()
    ax = fig.gca()
    ax.ticklabel_format(style='sci', axis='both', scilimits=(0,0))
    image = ax.imshow(demf,interpolation='nearest',extent=(X[0,0],X[0,-1],Y[0,0],Y[-1,0]))
    fig.colorbar(image)
    ax.set_title('Filled Elevation')
    plt.show()
    plt.savefig(filebase+'_dem_filled.png')

    #toy D8 flow algorithm
    Ms = d8(DX,DY,demf,route_flag=route_flag,use_mask=use_mask)
    #compute accumulated area
    As = flowacc(Ms,demf.shape)
    if use_mask:
        As = np.ma.masked_where(demf.mask,As)
    fig.clf()
    ax = fig.gca()
    ax.ticklabel_format(style='sci', axis='both', scilimits=(0,0))
    image = ax.imshow(As,interpolation='nearest',extent=(X[0,0],X[0,-1],Y[0,0],Y[-1,0]))
    fig.colorbar(image)
    ax.set_title('Accumulated Flow Area (number of cells)')
    plt.show()
    plt.savefig(filebase+'_flowarea.png')

    #compute basins
    from drainage import drainagebasins
    if use_mask:
        mask = demf.mask.reshape(demf.shape[0]*demf.shape[1],order='F')
        DB,outlets = drainagebasins(Ms,demf.shape,mask)
        DB = np.ma.masked_where(demf.mask,DB)
    else:
        DB,outlets = drainagebasins(Ms,demf.shape)
    fig.clf()
    ax = fig.gca()
    ax.ticklabel_format(style='sci', axis='both', scilimits=(0,0))
    image = ax.imshow(DB,interpolation='nearest',extent=(X[0,0],X[0,-1],Y[0,0],Y[-1,0]))
    fig.colorbar(image)
    ax.set_title('Drainage Basins')
    plt.show()
    plt.savefig(filebase+'_basins.png')

    return demf,Ms,As,DB,outlets

