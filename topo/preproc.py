#! /usr/bin/env python

import numpy as np
from scipy import ndimage


import heapq
def flood_fill(test_array,use_masked_input=False):
    input_array = test_array#np.copy(test_array)
    input_rows, input_cols = input_array.shape
     
    #### Set h_max to a value larger than the array maximum to ensure
    ####   that the while loop will terminate
    h_max = np.max(input_array*2.0)
     
    #### Build mask of cells with data not on the edge of the image
    #### Use 3x3 square structuring element
    el = ndimage.generate_binary_structure(2,2).astype(np.int)
    if use_masked_input:
        inside_mask = ndimage.binary_erosion(
            np.logical_not(input_array), structure=el)
    else:
            inside_mask = ndimage.binary_erosion(
                ~np.isnan(input_array), structure=el)
    #mwf added
    edge_mask = ~inside_mask
    #### Initialize output array as max value test_array except edges
    output_array = input_array.copy()
    output_array[inside_mask]=h_max
     
    #### Build priority queue and place edge pixels into priority queue
    #### Last value is flag to indicate if cell is an edge cell
    put = heapq.heappush
    get = heapq.heappop
    fill_heap = [
        (output_array[t_row,t_col], int(t_row), int(t_col), 1)
        for t_row, t_col in np.transpose(np.where(edge_mask))]
    heapq.heapify(fill_heap)
 
    #### Iterate until priority queue is empty
    while 1:
        try: h_crt, t_row, t_col, edge_flag = get(fill_heap)
        except IndexError: break
        for n_row, n_col in [
            ((t_row-1),t_col), ((t_row+1),t_col),
            (t_row,(t_col-1)), (t_row,(t_col+1))]:
            #### Skip cell if outside array edges
            if edge_flag:
                try:
                    if not inside_mask[n_row, n_col]: continue
                except IndexError: continue
            if output_array[n_row, n_col]==h_max:
                output_array[n_row, n_col] = max(
                    h_crt, input_array[n_row, n_col])
                put(fill_heap, 
                    (output_array[n_row, n_col], n_row, n_col, 0))
    return output_array
