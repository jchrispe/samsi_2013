#! /usr/bin/env python
import numpy as np

def flowpath(M,i_c):
    """
    compute indices of cells downstream from cell with linear index c

      \mat{M} \in R^{nxn}
      M_{ij} = 1 if cell i flows to cell j, zero otherwise

      Note, M_{ii} = 0, and \sum_{i} M_{ij} = number of cells that flow to j 

    First solve for cells downstream of c

     x_i = 1 if there exists path (directed edge set from c to i)

     x_c = 1
     x_i = 1 for immediate downstream neighbor i of c
     x_k = 1 for immediate downstream neighbor of i
     ...
    In other words solve

     x_i = \sum_{j} M_{ji}x_j + \delta_{ic}

    Second, solve for number of steps to reach cell k from cell i

     y_i = 0   i not downstream from c
     y_i = 1 + y_k if k --> i and k on path from c.

    Note that if k --> i and n --> i, c can not flow to both i and n

    In other words, solve

     y_i = x_i + \sum_{j} M_{ji} y_j

    With \vec y,

      extract nonzero entries, sort these with their indices re

    Input
      M sparse flow matrix size with logical size nxn
      


    """
    from scipy import sparse as sp
    from scipy.sparse import linalg as splg
    nc = M.shape[1]
    I = sp.eye(M.shape[0],M.shape[1])
    B = I-M.transpose()
    b = np.zeros(nc,'d')
    b[i_c] = 1
    x = splg.spsolve(B,b)
    y = splg.spsolve(B,x)
    y = np.asarray(y,'i')
    ind = np.argsort(y)
    order = ind[y[ind] > 0]

    return order


