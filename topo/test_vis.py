#! /usr/bin/env python

import numpy as np
from raster_input import *


    
def test_plot(filename):
    import matplotlib.pyplot as plt

    #nc defines number of cells in x direction
    #nr defines number of cells in y direction
    nc,nr,xll,yll,dx,nodata = read_raster_header(filename)
    X,Y,data = read_raster(filename,mask=True)
    
    fig = plt.figure()
    
    ax = fig.gca()
    ax.ticklabel_format(style='sci', axis='both', scilimits=(0,0))

    image = ax.imshow(data,interpolation='nearest',extent=(xll,xll+dx*nc,yll,yll+dx*nr))
    
    plt.show()
    plt.savefig(filename.split('.')[0]+'.png')

def test_animation(filebase,verbose=1):
    #setup animation
    import matplotlib
    matplotlib.use("Agg")
    import matplotlib.pyplot as plt
    import matplotlib.animation as animation
    
    #mwf debug
    #import pdb
    #pdb.set_trace()
    #Writer = animation.writers['ffmpeg']
    #writer = Writer(fps=15,metadata=dict(artist='Foo'),bitrate=1800)

    #generate all of the figures
    fig = plt.figure()
    

    import glob
    files = glob.glob(filebase+'.*[0-9]')
    files_to_remove = []
    for name in files:
        if name.split('.')[-1] == 'mp4':
            files_to_remove.append(name)
    for name in files_to_remove:
        files.remove(name)
    def suff_comp(x,y):
        sx = int(x.split('.')[-1])    
        sy = int(y.split('.')[-1])
        if sx < sy:
            return -1
        if sx > sy:
            return 1
        return 0
    #
    files.sort(cmp=suff_comp)
    if verbose > 0:
        print "trying to read {} files with base {}".format(len(files),filebase)   
    #store images here
    ax = fig.gca()
    ax.ticklabel_format(style='sci', axis='both', scilimits=(0,0))
    ims = []
    for file_step in files:
        #nc defines number of cells in x direction
        #nr defines number of cells in y direction
        if verbose > 0:
            print "reading {}".format(file_step)
        X,Y,data = read_raster(file_step,mask=True)
 
        ims.append([plt.imshow(data,interpolation='nearest',
                             extent=(xll,xll+dx*nc,yll,yll+dx*nr))])
    #end plot loop
    #mwf debug
    #import pdb
    #pdb.set_trace()
    im_ani = animation.ArtistAnimation(fig,ims,interval=200,repeat_delay=3000,blit=True)
    im_ani.save(filebase+'.mp4')#writer=writer)
                

            
if __name__ == "__main__":

    import os,optparse

    usage = "usage: %prog map_file_to_print"
    parser = optparse.OptionParser(usage)
    parser.add_option("-a","--animation",
                      help="Generate an animation",
                      action="store_true",
                      default=False)
    
    (opts,args) = parser.parse_args()
    if len(args) < 1:
        print "Too few arguments: {}. Must specify name of file to plot".format(args)
        sys.exit(1)
    if not opts.animation:
        test_plot(args[0])
    else:
        test_animation(args[0])
        
