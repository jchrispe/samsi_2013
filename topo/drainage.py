#! /usr/bin/env

import numpy as np
from scipy import sparse as sp
from scipy.sparse import linalg as splg

def drainagebasins(M,dsh,mask=None):
    """
    Given flow direction matrix, M, compute drainage basins

     x_i = index of the drainage basin to which cell i belongs
    
    
    Input:
      M -- flow matrix, M_{ij} = 1 if cell 1 flows to cell j. zero otherwise
      dsh -- logical size of dem


    
    1. Compute drainage cells which are cells that receive water but do not route water. 
      Cell i is an outlet if
    
        \sum_{j} M_{ij} = 0 and \sum_{i} M_{ij} > 0
 
    2. Generate a unique enumeration for outlets, \vec n,
       n_i = 0 if i is not an outlet
           = unique outlet index otherwise
           
    3. Determine outlet to which each cell belongs. Build system noting

       Cell i belongs to outlet k if M_{ik} = 1 or M_{i'k} = 1 for some cell downstream from i

       Cell i must belong to same outlet as its downstream neighbor

       Cell i is in its own basin if it's an outlet

       x_i = n_i if i is a basin
       x_i = x_j otherwise (in the same basin as its downstream neighbor)

       In other words, 

       x_i = n_i + \sum_{j}M_{ij}x_j

       or
       (\mat{I}-\mat{M})\vec x = \vec n

    4. Remove isolated cells (do not receive or route water) from the basins
    
    """

    ntot = dsh[0]*dsh[1]
    assert M.shape[0] == ntot and M.shape[1] == ntot
    if mask != None:
        assert M.shape[0] == mask.shape[0]
    ##find cells that receive water only
    #\sum_{i} M_{ij}
    routes   = M.sum(axis=1)  #1 if cell routes flow to another cell, 0 otherwise
    #\sum_{i} M_{ij}
    receives = M.sum(axis=0)  # number of neighbors that flow to each cell
    #cells that don't route flow but also receive it
    index = np.logical_and(routes.flat == 0,receives.flat != 0)
    if mask != None:
        index = np.logical_and(index, ~mask)

    drains = np.zeros(ntot,'d')
    drains[index] = 1

    #grab cells that are drainage outlets
    outlets = np.where(drains == 1)
    
    drains = np.cumsum(drains)*drains

    #solve flow to drains
    I = sp.eye(ntot,ntot)
    A = I-M

    X = splg.spsolve(A,drains)

    
    index_orph = np.logical_and(routes.flat == 0, receives.flat == 0)
    X[index_orph] = -1
    index_orph = np.logical_not(index_orph)
    x,iix = np.unique(X[index_orph],return_inverse=True)
    X[index_orph] = iix
    
    
    X = X.reshape(dsh,order='F')
    
    
    return X,outlets


