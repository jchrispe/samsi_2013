#! /usr/bin/env python

import numpy as np

def read_raster_header(filename):
    """
    read an ascii raster file header and return 

ncols		number of columns in raster grid
nrows		number of rows in raster grid
xllcorner	bounding box reference
yllcorner	
cellsize	dx=dy
NODATA_value	value means not modelled
 
value           numpy array of shape (nrows,ncols)
    """
    fd = open(filename,'r')

    line = fd.readline()
    ncols = int(line.split()[1])
    
    line = fd.readline()
    nrows = int(line.split()[1])

    line = fd.readline()
    xll   = float(line.split()[1])

    line = fd.readline()
    yll   = float(line.split()[1])

    line = fd.readline()
    dx    = float(line.split()[1])

    line = fd.readline()
    NODATA_value = float(line.split()[1])

    fd.close()

    return ncols,nrows,xll,yll,dx,NODATA_value

def read_raster_data(filename):
    """
    read a ascii raster file return 

    value           numpy array of shape (nrows,ncols)

    """
    
    value = np.loadtxt(filename,skiprows = 6)
    return value

    
def read_raster(filename,mask=False):
    """
    read a raster file and return
      X,Y, data

    where X,Y are output from meshgrid

    if mask = true returns np masked array for NODATA_value entries

    """
    #nc defines number of cells in x direction
    #nr defines number of cells in y direction
    nc,nr,xll,yll,dx,nodata = read_raster_header(filename)
    data = read_raster_data(filename)
    if mask:
        data = np.ma.masked_where(data == nodata,data)
    #note reversal of nc, nr
    X = np.arange(xll,xll+dx*nc,dx)
    Y = np.arange(yll,yll+dx*nr,dx)
    X,Y = np.meshgrid(X,Y)

    return X,Y,data


