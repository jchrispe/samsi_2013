#! /usr/bin/env python

import numpy as np
from collections import deque

def resolve_flats(dem,M,verbose=0,use_mask=False):
    """
    Try to implement Barnes, Lehman, etal 13 flat resolution algorithm (alg. 1) based on Garbrecht and Martz

    Input:
      dem -- original dem
      M   -- flowdirection matrix already determined (using d8 for now)

    Note column major correspondence between (i,j) and glocal index for cell

       C = j*nr + i

    Output:
       flat_mask
       flat_labels
    return_flag:
       
    """
    
    nr = dem.shape[0]; nc = dem.shape[1]
    neighbors = build_neighbors(dem)
    flat_mask = np.zeros(nr*nc,'d'); flat_labels = np.zeros(nr*nc,'i')

    #build FIFO queues for flat cells bordering high cells and flat cells bordering lower cells
    #Note
    # stores global index C
    # deque build FIFO queues using append and popleft
    
    high_edges,low_edges = find_flat_edges(dem,M,neighbors,use_mask=use_mask)

    if len(low_edges) == 0:
        if len(high_edges) > 0:
            print "Undrainable flats found low_edges= {}, high_edges= {} ".format(low_edges,high_edges)
        elif len(low_edges) == 0 and len(high_edges) == 0:
            print "No flats found"
        return flat_mask,flat_labels,None,neighbors
    #
    label_id = 1
    for C in low_edges:
        if flat_labels[C] == 0:
            label_flats(dem,neighbors,flat_labels,C,label_id)
            label_id += 1
        #
    #    
    no_outlet = False
    unique_high_edges = deque()
    for C in high_edges:
        if flat_labels[C] > 0:
            unique_high_edges.append(C)
        else:
            no_outlet = True
    #

    flat_height = np.zeros(label_id-1,'d')
    assert len(flat_height) == flat_labels.max()
    away_from_higher(M,neighbors,unique_high_edges,flat_labels,flat_mask,flat_height)

    towards_lower(M,neighbors,low_edges,flat_labels,flat_mask,flat_height)
    return flat_mask,flat_labels,flat_height,neighbors

def find_flat_edges(dem,M,neighbors,use_mask=False):
    """

    Input:
      dem -- original dem
      M   -- flowdirection matrix already determined (using d8 for now)

    Note column major correspondence between (i,j) and glocal index for cell

       C = j*nr + i

    Output:
      FIFO queues for flat cells bordering high cells and flat cells bordering lower cells

    Notes:
     stores global index C
     deque build FIFO queues using append and popleft

     neighbor convention

     3  2  1          (i-1,j-1)  (i-1,j) (i-1,j+1)                                      
     4     0     ---> (i  ,j-1)          (i  ,j+1)  , matlab style convention (i,j) --> j*dem.shape[0] + i  --> 
     5  6  7          (i+1,j-1)  (i+1,j) (i+1,j+1)

    """
    low_edges = deque()
    high_edges= deque()
    
    nr = dem.shape[0]; nc = dem.shape[1]
    elev = dem.reshape(nr*nc,order='F')
    if use_mask:
        mask = dem.mask.reshape(nr*nc,order='F')
    else:
        mask = np.zeros(nr*nc,dtype='b')
        
    neig_incr_col_major = np.array([nr,
                                    nr-1,-1,-nr-1,
                                    -nr,
                                    -nr+1,1,nr+1])
    #no flow cells
    b = np.ones(nc*nr,'d')
    routes = M*b
    eps = 1.0e-8

    for C in range(nc*nr):
        if mask[C]:
            continue
        #only interior for now
        for N in neighbors[C]:
            if 0 <= N and not mask[N]:
                if routes[C] > eps and routes[N] < eps and abs(elev[C]-elev[N]) < eps:
                    low_edges.append(C)
                    break
                elif routes[C] <= eps and elev[C] < elev[N]:
                    high_edges.append(C)
                    break
            #
        #
    #
        
    #for i in range(1,nr-1):
    #    for j in range(1,nc-1):
    #        C = j*nr + i
    #        neigs = C + neig_incr_col_major
    #        assert neigs.min() >= 0
    #        assert neigs.max() < nr*nc
    #        n_elev  = elev[neigs]
    #        imax    = n_elev.argmax()
    #        n_routes= routes[neigs]
    #        #no flow and elevenation is lower than some neighbor
    #        if routes[C] < eps and elev[C] < n_elev[imax]:
    #            high_edges.append(C)
    #        elif routes[C] > eps and n_routes.min() == 0:
    #            #this cell has a gradient but one of its neighbors does not
    #            low_edges.append(C)
    #    #
    #

    return high_edges,low_edges

def build_neighbors(dem):
    """
    Build brute force neighbor array for each cell in dem

    neighbor[C,k] = global_id of kth neighbor of C or -1 if not in dem
    
     neighbor convention

     3  2  1          (i-1,j-1)  (i-1,j) (i-1,j+1)                                      
     4     0     ---> (i  ,j-1)          (i  ,j+1)  , matlab style convention (i,j) --> j*dem.shape[0] + i  --> 
     5  6  7          (i+1,j-1)  (i+1,j) (i+1,j+1)

    """
    nr = dem.shape[0]; nc =dem.shape[1]
    neighbors = np.zeros((nr*nc,8),'i')
    neighbors.fill(-1)
    indeces = np.arange(nr*nc,dtype='i').reshape((nr,nc),order='F')
    neig_incr_col_major = np.array([nr,
                                    nr-1,-1,-nr-1,
                                    -nr,
                                    -nr+1,1,nr+1])


    for i in range(1,nr-1):
        for j in range(1,nc-1):
            C = j*nr + i
            neighbors[C,:] = C + neig_incr_col_major
        #
    #
    for j in range(1,nc-1):
        C = j*nr+0
        for k in [0,4,5,6,7]:
            neighbors[C,k] = C + neig_incr_col_major[k]
        C = j*nr+ nr-1
        for k in [0,1,2,3,4]:
            neighbors[C,k] = C + neig_incr_col_major[k]
    for i in range(1,nr-1):
        C = 0*nr + i
        for k in [0,1,2,6,7]:
            neighbors[C,k] = C + neig_incr_col_major[k]
        C = (nc-1)*nr + i
        for k in [2,3,4,5,6]:
            neighbors[C,k] = C + neig_incr_col_major[k]
    C = 0*nr + 0;
    for k in [0,6,7]:
        neighbors[C,k] = C + neig_incr_col_major[k] 
    C = 0*nr + nr-1
    for k in [0,1,2]:
        neighbors[C,k] = C + neig_incr_col_major[k]
    C = (nc-1)*nr + 0;
    for k in [4,5,6]:
        neighbors[C,k] = C + neig_incr_col_major[k] 
    C = (nc-1)*nr + nr-1
    for k in [2,3,4]:
        neighbors[C,k] = C + neig_incr_col_major[k]

    #
    return neighbors

def label_flats_brute(dem,neighbors,labels,C,label_id):
    """
    Input:
      dem -- original dem flattened in linear array consistent with M
      labels -- holds unique id of flat region for each cell (0 for cells not in flat)
      C      -- cell belonging to flat being labeled
      label_id -- unique id for this flat
      
    Output:
      C and all cells reachable from C while staying in the same flat have been assigned label_id in
        labels array
    Note column major correspondence between (i,j) and glocal index for cell

       C = j*nr + i

   
    """
    nr = dem.shape[0]; nc = dem.shape[1]
    tofill = deque()
    tofill.append(C)
    unlabeled = 0
    #unpack linear index
    i = C % nr ; j = int(C/nr)
    z = dem[i,j]
    eps = 1.0e-8
    while len(tofill) > 0:
        CC = tofill.popleft()
        assert 0 <= CC and CC < nr*nc
        ii = CC % nr; jj = int(CC/nr)
        if abs(dem[ii,jj] - z) < eps and labels[CC] == unlabeled:
            labels[CC] = label_id
            for N in neighbors[CC]:
                if N >= 0:
                    tofill.append(N)
                
            #really slow
            #if ii > 0: tofill.append(jj*nr + ii-1)
            #if jj > 0: tofill.append((jj-1)*nr + ii)
            #if ii < nr-1: tofill.append(jj*nr + ii+1)
            #if jj < nc-1: tofill.append((jj+1)*nr + ii)
            #if ii > 0 and jj > 0: tofill.append((jj-1)*nr + ii-1)
            #if ii > 0 and jj < nc-1: tofill.append((jj+1)*nr + ii-1)
            #if ii < nr-1 and jj > 0: tofill.append((jj-1)*nr + ii+1)
            #if ii < nr-1 and jj < nc-1: tofill.append((jj+1)*nr + ii+1)
        #    
    #

label_flats = label_flats_brute

def away_from_higher(M,neighbors,high_edges,flat_labels,flat_mask,flat_height):
    """
    Input:
      M -- flow matrix
      neighbors -- neighbor array (-1 is invalid)
      high_edges -- indexes for cells in flat but next to regions of higher elevation
      flat_labels -- array indexed by global index with
         0 ==   -- cell not a flat
         0  <   -- unique id of flat to which cell belongs
      flat_mask -- linear array initialized to 0

    Output:
      flat_height -- height assigned to each flat (number of flats long)
      flat_mask -- counter for each cell in process away from higher elevations
    """
    #no flow cells
    b = np.ones(M.shape[1],'d')
    routes = M*b
    no_flow = 0
    
    loops = 1

    marker = -1
    high_edges.append(marker)
    
    while len(high_edges) > 1:
        C = high_edges.popleft()
        if marker == C:
            loops += 1
            high_edges.append(marker)
        elif flat_mask[C] == 0:
            flat_mask[C] = loops
            flat_height[flat_labels[C]-1] = loops
            for N in neighbors[C]:
                if 0 <= N and flat_labels[N] == flat_labels[C] and no_flow == routes[N]:
                    high_edges.append(N)
                #
            #
        #
    #

def towards_lower(M,neighbors,low_edges,flat_labels,flat_mask,flat_height):
    """
    Input:
      M -- flow matrix
      neighbors -- neighbor array (-1 is invalid)
      low_edges -- indexes for cells in of lower elevation neighboring flats
      flat_labels -- array indexed by global index with
         0 ==   -- cell not a flat
         0  <   -- unique id of flat to which cell belongs
      flat_mask -- linear array
         0 ==   -- cell not a flat
         0 <    -- number of increments needed to add to form gradient away from higher elev.
      
    Output:
      flat_height -- height assigned to each flat (number of flats long)
      flat_mask -- number of increments to add to each cell in combined low+high gradient process
 
    """
    #no flow cells
    b = np.ones(M.shape[1],'d')
    routes = M*b
    no_flow = 0

    flat_mask *= -1
    loops = 1
    marker = -1

    low_edges.append(marker)

    while len(low_edges) > 1:
        C = low_edges.popleft()
        if marker == C:
            loops += 1
            low_edges.append(marker)
        else:
            if flat_mask[C] > 0:
                continue
            if flat_mask[C] < 0.:
                flat_mask[C] += flat_height[flat_labels[C]-1] + 2.*loops
            elif flat_mask[C] == 0:
                flat_mask[C] = 2.*loops
            for N in neighbors[C]:
                if 0 <= N and flat_labels[N] == flat_labels[C] and no_flow == routes[N]:
                    low_edges.append(N)
                #
            #
        #
    #

def adjust_flow_matrix_d8(M,neighbors,flat_mask,flat_labels):
    """

    """
    #no flow cells
    b = np.ones(M.shape[1],'d')
    routes = M*b
    no_flow = 0

    cells_to_check = np.where(routes == 0)[0]

    for C in cells_to_check:
        e_min = flat_mask[C]
        n_min = -1
        for N in neighbors[C]:
            if 0 <= N and flat_labels[N] == flat_labels[C] and flat_mask[N] < e_min:
                e_min = flat_mask[N]
                n_min = N
        if n_min >= 0:
            M[C,n_min] = 1
    #

def adjust_dem(dem,neighbors,flat_mask,flat_labels):
    """

    """
    failed = False
    nr = dem.shape[0]; nc = dem.shape[1]
    dem_out = dem.reshape(nr*nc,order='F')
    #mwf debug
    import pdb
    pdb.set_trace()
    cells_to_check = np.where(flat_mask > 0)[0]
    higher = np.zeros(neighbors.shape[1],'b')
    for C in cells_to_check:
        for k,N in enumerate(neighbors[C]):
            higher[k] = False
            if 0 <= N:
                higher[k] = dem_out[C]-dem_out[N]
        for j in range(int(flat_mask[C])):
            dem_out[C] = np.nextafter(dem_out[C],np.inf)
        for k,N in enumerate(neighbors[C]):
            if (0 <= N and flat_labels[N] != flat_labels[C] and
                dem_out[C] > dem_out[N] and not higher[k]):
                failed = True
                i = C % nr; j = int(C/nr)
                print "WARNING: altered elevation of cell {} from {} to {} now higher than {} with elevation {}".format(C,dem[i,j],dem_out[C],N,dem_out[N])
            #
        #
    #
    return failed, dem_out

def test_gb():

    import flowdir as fd
    from raster_input import read_raster
    filebase = 'garbrecht_canyon'
    X,Y,dem = read_raster('garbrecht_canyon.dem')
    DY = DX = X[0,1]-X[0,0]

    dem,Ms,As,DB,outlets = fd.test1('garbrecht_canyon.dem')
    
    flat_mask,flat_labels = resolve_flats(dem,Ms)
    neigs = build_neighbors(dem)
    import copy
    Ms_new = copy.deepcopy(Ms)
    adjust_flow_matrix_d8(Ms_new,neigs,flat_mask,flat_labels)

    #compute accumulated area
    As_new = fd.flowacc(Ms_new,dem.shape)

    import matplotlib.pyplot as plt
    fig = plt.figure()

    fig.clf()
    ax = fig.gca()
    ax.ticklabel_format(style='sci', axis='both', scilimits=(0,0))
    image = ax.imshow(As_new,interpolation='nearest',extent=(X[0,0],X[0,-1],Y[0,0],Y[-1,0]))
    plt.show()
    plt.savefig(filebase+'_new_flowarea.png')

    #compute basins
    from drainage import drainagebasins
    DB_new,outlets_new = drainagebasins(Ms_new,dem.shape)
    
    fig.clf()
    ax = fig.gca()
    ax.ticklabel_format(style='sci', axis='both', scilimits=(0,0))
    image = ax.imshow(DB_new,interpolation='nearest',extent=(X[0,0],X[0,-1],Y[0,0],Y[-1,0]))
    plt.show()
    plt.savefig(filebase+'_new_basins.png')

    return flat_mask,flat_labels,As_new,DB_new,outlets_new


    
