To Do list for Managed Aquifer Recharge Work
--------------------------------------------

1. Revisit SAMSI work

   a. Run Travis and Mays (TM) example with cython code, check results versus TM paper

   b. Run Watershed delineation tools on Pajaro DEM, check output versus SAMSI result
 
   c. Run CASC2D for Pajaro problem

      i. What output was collected (water depth, total run off and infiltration at basins)?

      ii. Confirm with Sourav how he used the CASC2D results for optimization (also where did infiltration factor come from)?

   d. Find out how MATLAB interior point optimization was run   


2. Test out SAMSI approach by running it on a new location (first option Onslow County, NC)

   a. Download DEM convert to ascii 

   b. Run topotoolbox script to generate watershed basins, stream network, flow accumulation

   c. Setup CASC2D to run on the domain

   d. Pick target basins, compute total runoff and infiltration in those cells from CASC2D simulation

   e. Use this information in network based optimization with recharge-based objective(s) and constraints


3. Allow optimizer to pick recharge locations as part of the optimization

   a. Setup CASC2D to run inside the optimization loop

   b. Add local recharge basin model to CASC2D
      
      i. Need to figure out way to add a term to CASC2D right hand side to account for increased recharge from basin in the cell (added ponded water depth, higher K value in basin)


4. Pre-run surface-water and groundwater problem on the domain to determine infiltration suitability index:
   
   a. Start with toy subsurface stratigraphy: 'intermittent' confining layer between surficial aquifer and deeper source aquifer. 

   b. Apply constant source to selected cells, compute efficiency as Recharge Volume / Target Aquifer Storage Increase ?
