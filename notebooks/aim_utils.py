#! /usr/bin/python
import os
from osgeo import gdal
import numpy as np

def read_raster(filename,band_number=1):
    """
    just wraps gdal commands for reading a raster

    returns dataset, and masked array
    """
    assert os.path.isfile(filename) or os.path.isdir(filename), "could not find {0}".format(filename)

    dataset = gdal.Open(filename,gdal.GA_ReadOnly) #open the file

    nbands = dataset.RasterCount #number of bands in raster
    assert band_number <= nbands and band_number >= 1

    band = dataset.GetRasterBand(band_number)

    raw_data = band.ReadAsArray(0, 0, dataset.RasterXSize, dataset.RasterYSize).astype(np.float)

    ma_data = np.ma.masked_where(np.abs(raw_data-band.GetNoDataValue()) <= 1.0e-2*np.abs(band.GetNoDataValue()),raw_data)

    return dataset, ma_data
